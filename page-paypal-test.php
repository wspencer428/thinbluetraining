<?php
/**
 * page-paypal-test.php
 *
 * @package bootstrapped
 */

get_header(); 
if ( !is_user_logged_in() ) {
    header('Location: ' . wp_login_url());
}?>
	<div class="row">
		<div class="col-md-7">
		<!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="GHYE3SFTG7Z82">
			<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
		</form> -->

        <form method="post" action= "https://www.sandbox.paypal.com/cgi-bin/webscr">
		        <input type="hidden" name="cmd" value="_xclick">
		        <input type="hidden" name="item_name" value="Order">
		        <input type="hidden" name="business" value="wspencer428-facilitator@hotmail.com">
		        <input type="hidden" id="item_number" name="item_number" value="123">
		        <input type="hidden" name="amount" value="0.01">
		        <input type="hidden" name="no_shipping" value="1">
		        <input type="hidden" name="rm" value="1">
		        <input type="hidden" name="custom" value="This is a test">
		        <input type="hidden" name="return" value="http://clients.willspencerdesign.com/ThinBlueTraining/success" />
		        <input type="hidden" name="hosted_button_id" value="GHYE3SFTG7Z82">
				<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
		        <!-- <input type="hidden" name="cancel_return" value="http://www.blah.com/checkout/cancel" /> -->
		</form> 

        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>