<?php
/**
 * single-event.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            <strong>Description: </strong><?php the_content(); ?>
	            <?php
	            	$date = gmdate("m/d/Y", get_post_meta( get_the_ID(), '_wsdev_event_date', true));
	            	$address = get_post_meta( get_the_ID(), '_wsdev_event_address', true );
	            	$city = get_post_meta( get_the_ID(), '_wsdev_event_city', true );
	            	$state = get_post_meta( get_the_ID(), '_wsdev_event_state', true );
	            	$zip = get_post_meta( get_the_ID(), '_wsdev_event_zip', true );
	            	$cost = get_post_meta( get_the_ID(), '_wsdev_event_cost', true );
	            	$website = get_post_meta( get_the_ID(), '_wsdev_event_website', true );
	            	$contact_person = get_post_meta( get_the_ID(), '_wsdev_event_contact_person', true );
	            	$contact_email = get_post_meta( get_the_ID(), '_wsdev_event_contact_email', true );
	            	$contact_phone = get_post_meta( get_the_ID(), '_wsdev_event_contact_phone', true );
	            	$topics = get_post_meta( get_the_ID(), '_wsdev_event_topic', true );
	            ?>
	            <ul class="details-list">
	            	<li><strong>Date:</strong> <?php echo $date; ?></li>
	            	<li><strong>Address:</strong> <?php echo $address; ?>&nbsp;<?php echo $city; ?>, <?php echo $state; ?>&nbsp;&nbsp;<?php echo $zip; ?></li>
	            	<li><strong>Cost:</strong> $<?php echo $cost; ?></li>
	            	<li><strong>Website:</strong> <a href="<?php echo $website; ?>"><?php echo $website; ?></a></li>
	            	<li><strong>Contact Person:</strong> <?php echo $contact_person; ?></li>
	            	<li><strong>Contact E-mail:</strong> <?php echo $contact_email; ?></li>
	            	<li><strong>Contact Phone:</strong> <?php echo $contact_phone; ?></li>
	            	<li><strong>Topics:</strong> <?php 
	            						if ( $topics ) {
	            							foreach ($topics as $topic) { 
	            								if ($topic != '') {
	            									echo $topic.', '; 
	            								}
	            							} 
	            						}
	            						 ?></li>
	            </ul>
        	<?php endwhile; ?>
        	<?php 
        		if ( is_user_logged_in() ) {
					$current_user = wp_get_current_user();
					$user_info = get_userdata($current_user->ID);
					if ( in_array('trainer', $user_info->roles) || get_the_author_meta('user_type', $current_user->ID) == 'trainer') {
						$is_trainer = true;
					}
					else {
						$is_trainer = false;
					}

					if ($is_trainer) {
							
						
						echo '<div class="row" style="margin-bottom: 20px; margin-top: 20px; text-align: center;"><div class="col-xs-6"><a style="color: #FFFFFF;" href="'.get_site_url().'/post-class" class="btn btn-primary btn-lg">Post another class</a></div>';
						echo '<div class="col-xs-6"><a style="color: #FFFFFF;" href="'.get_site_url().'/view-listings" class="btn btn-primary btn-lg">View existing listings</a></div></div>';
						
					}
				}
			?>
			<div class="row">
				<div class="col-xs-12">
					<p>Need any assistance? We're here to help. <a href="mailto:service@thinbluetraining.com">service@thinbluetraining.com</a></p>
				</div>
			</div>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>