<?php
/**
 * archive-events.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	
	        <h1>Calendar</h1>
	        <!-- <div class="alert alert-info" role="alert">Have an event you'd like listed on Thin Blue Training? <a href="<?php bloginfo('url'); ?>/post-class">Click here!</a></div> -->
	        <?php include(locate_template('partials/listing-calendar.php')); ?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>