<?php
/**
 * page-purchase-listings.php
 *
 * @package bootstrapped
 */

get_header(); 
if ( !is_user_logged_in() ) {
    header('Location: ' . wp_login_url());
}?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            
	            <?php the_content(); ?>

	            <div class="form-group" style="margin-top: 25px;">
					<label class="col-sm-8 control-label">Select how many class listings you want to list</label>
					<div class="col-sm-4">
    					<input type="number" min="0" class="form-control" max-length="3" id="numberListings" name="number_listings" />
    					<div class="help-block">Total amount: <span class="totalPrice"></span></div>
    				</div>
				</div>

				<div class="form-group" style="margin-top: 25px;">
					<h3 style="text-align: center;">Want YOUR class to stand out? <strong>Bold it!</strong> ($15)</h3>
					<label class="col-sm-8 control-label">How many BOLD listings?</label>
					<div class="col-sm-4">
    					<input type="number" min="0" class="form-control" max-length="3" id="numberBoldListings" name="number_bold_listings" />
    					<div class="help-block">Total amount: <span class="totalPrice"></span></div>
    				</div>
				</div>

				<div id="couponFields" class="form-group" style="margin-top: 25px;">
					<h3 style="text-align: center;">Have a coupon code? Enter it here.</h3>
					<label class="col-sm-8 control-label" style="text-align: right;">Coupon code</label>
					<div class="col-sm-4">
    					<input type="text" class="form-control" max-length="10" id="couponCode" name="coupon_code" />
    					<input type="hidden" id="couponCodeHidden" val="" />
    					<button id="applyCouponButton" class="btn btn-primary btn-lg" style="margin: 10px;" disabled>Apply Coupon</button>
    				</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<alert id="couponStatus" class="alert" style="display: none;"></alert>
					</div>
				</div>

				<input type="hidden" id="url" value="<?php bloginfo('stylesheet_directory'); ?>" />

				<div class="form-group" style="margin-top: 25px; text-align: center;">
					<div id="singleListingButton">
						<form method="post" action= "https://www.paypal.com/cgi-bin/webscr">
						<!-- <form method="post" action= "https://www.sandbox.paypal.com/cgi-bin/webscr"> -->
					        <input type="hidden" name="cmd" value="_xclick">
					        <input type="hidden" name="item_name" value="Order">
					        <input type="hidden" name="amount" value="">
					        <input type="hidden" name="no_shipping" value="1">
					        <input type="hidden" name="rm" value="1">
					        <input type="hidden" name="custom" value="">
					        <!-- sandbox -->
					        <!-- <input type="hidden" name="business" value="wspencer428-facilitator@hotmail.com">
					        <input type="hidden" name="hosted_button_id" value="GHYE3SFTG7Z82">
					        <input type="hidden" name="return" value="http://clients.willspencerdesign.com/ThinBlueTraining/success" />
					        <input type="hidden" name="cancel_return" value="http://clients.willspencerdesign.com/ThinBlueTraining/purchase-listings" /> -->
					        <!-- production -->
					        <input type="hidden" name="hosted_button_id" value="CWLXVL3XY35ZQ"> 
					        <input type="hidden" name="business" value="sholloway@hollotec.com">
					        <input type="hidden" name="return" value="http://www.thinbluetraining.com/success" />
					        <input type="hidden" name="cancel_return" value="http://www.thinbluetraining.com/purchase-listings" />
							<input type="submit" name="submit" alt="Buy listings!" value="Continue" class="btn btn-primary btn-lg" disabled>
							<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
						</form> 

					</div>
					<!-- <a class="btn btn-primary btn-lg" href="" style="color: #FFFFFF;">Buy listings!</a> -->
				</div>

				<script>
				jQuery('#numberListings, #numberBoldListings').change(function() {
					updatePrice();
				});

				function updatePrice(discount) {

					var number = jQuery('#numberListings').val();
					var boldNumber = jQuery('#numberBoldListings').val();
					var couponCode = jQuery('#couponCodeHidden').val();

					var price;
					var listingCost;
					var boldCost = 0;
					var totalCost;

					if (number <= 5) {
						price = 35;
						//listingCost = number * 35;
					}
					
					if (number > 5 && number <= 20) {
						price = 30;
						//listingCost = number * 30;
					}

					if (number > 20) {
						price = 20;
						//listingCost = number * 20;
					}

					if (boldNumber >= 1) {
						boldCost = boldNumber * 15;
					}

					listingCost = number * price;

					totalCost = boldCost + listingCost;

					if (discount) {

						if (discount == 100) {
							console.log('free');
						}
						var discountNumber = discount * .01;
						var priceOff = price * discountNumber;
						totalCost = totalCost - priceOff;
					}

					jQuery('.totalPrice').text('$ '+totalCost.toFixed(2));

					jQuery('input[name="amount"]').val(totalCost);
					jQuery('input[name="custom"]').val(number+','+boldNumber+','+couponCode);

					if (number > 0) {
						jQuery('input[name="submit"]').prop('disabled', false);
						jQuery('#applyCouponButton').prop('disabled', false);
					}
					else {
						jQuery('input[name="submit"]').prop('disabled', true);
						jQuery('#applyCouponButton').prop('disabled', true);
					}
				}

				function applyCoupon() {

					var url = jQuery('#url').val();
					var couponCode = jQuery('#couponCode').val();

					jQuery('#couponCodeHidden').val(couponCode);

					$.ajax({
						type: 'GET',
						url: url+'/coupon-code.php',
						data: "couponCode="+couponCode,
						success: function(result) {

							switch (result) {

								case 'You forgot to enter your coupon code.':
									jQuery('#couponStatus').show().addClass('alert-danger').text(result);
									break;

								case 'The coupon code you entered is not valid.':
									jQuery('#couponStatus').show().addClass('alert-danger').text(result);
									break;

								case 'The coupon code you entered has expired.':
									jQuery('#couponStatus').show().addClass('alert-danger').text(result);
									break;

								case 'The coupon code you entered is no longer valid.':
									jQuery('#couponStatus').show().addClass('alert-danger').text(result);
									break;

								case '100':
									window.location.href = '<?php bloginfo("url"); ?>/success/?couponCode='+couponCode;
									break;

								default:
									jQuery('#couponStatus').show().removeClass('alert-danger').addClass('alert-success').text("Your coupon code was successfully added!");

									updatePrice(parseInt(result));
									jQuery('#couponFields').hide();
									break;
							}
							
						},
						error: function() {
							jQuery('#couponStatus').show().addClass('alert-danger').text('Sorry, we can\'t complete your request at this time.');
						}
					});
				}

				jQuery(document).ready(function() {
					jQuery('#applyCouponButton').click(function() {
						applyCoupon();
					});
				});

				</script>

				

        	<?php endwhile; ?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>