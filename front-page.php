<?php
/**
 * front-page.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
			<h1>Calendar</h1>
			<?php include(locate_template('partials/listing-calendar.php')); ?>
		</div>
		
		<?php get_sidebar(); ?>
		
	</div>


<?php get_footer(); ?>