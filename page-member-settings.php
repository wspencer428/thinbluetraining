<?php
/**
 * page-member-settings.php
 *
 * @package bootstrapped
 */


	$current_user = wp_get_current_user();

	$user_info = get_userdata($current_user->ID);

	if ( in_array('trainer', $user_info->roles) || get_the_author_meta('user_type', $current_user->ID) == 'trainer' ) {
		$is_trainer = true;
	}
	else {
		$is_trainer = false;
	}

	if ( get_the_author_meta('user_type', $current_user->ID) == 'recruiter') {
		$is_recruiter = true;
	}
	else {
		$is_recruiter = false;
	}

	if (strpos($current_user->user_login, ' ') !== false) {
		$current_user->user_login = str_replace(" ", "-", $current_user->user_login);
	}

	if ( !is_user_logged_in() ) {
	    header('Location: ' . wp_login_url());
	} 


	if (isset($_GET['from']) && $_GET['from'] == 'alerts') {
		$trainerAlertsMessage = true;
	}
	else {
		$trainerAlertsMessage = false;
	}


	$displayTools = true;

	
	$display_name = get_the_author_meta('display_name', $current_user->ID);
	$email = get_the_author_meta('user_email', $current_user->ID);
	$phone = get_the_author_meta('user_phone', $current_user->ID);
	$zip = get_the_author_meta('user_zip', $current_user->ID);
	$sms_alert = get_the_author_meta('sms_alert', $current_user->ID);
	$email_alert = get_the_author_meta('email_alert', $current_user->ID);
	$alert_radius = get_the_author_meta('alert_radius', $current_user->ID);
		


	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "settings_posted") {

		if (wp_verify_nonce($_POST['settings_posted'], 'memberSettings')) {

			if ( isset($_POST['username']) ) {
		
				if ( $_POST['username'] != $current_user->user_login ) {
					wp_redirect( home_url() );
				}

				
		        if ( $_POST['user_email'] === '' ) {
		            $emailError = 'An email address is required.';
		            $hasError = true;
		        } else {
		            $user_email = $_POST['user_email'];
		        }


		        if ( isset($_POST['new_password']) && isset($_POST['password_confirm']) && !isset($_POST['current_password']) ) {

		        	$passwordError = 'You must enter your current password to change it. Please try again.';
				    $hasError = true;
		        } 

		        if ( isset($_POST['current_password']) && isset($_POST['new_password']) && !isset($_POST['password_confirm']) ) {

		        	if ($passwordError) {
	        			$passwordError = $passwordError.'<br />You must enter a new password and confirm it in the next field. Please try again.';
	        			$hasError = true;
	        		}
	        		else {
	        			$passwordError = 'You must enter a new password and confirm it in the next field. Please try again.';
			        	$hasError = true;
	        		}
		        }

		        if ( isset($_POST['current_password']) && isset($_POST['password_confirm']) && !isset($_POST['new_password']) ) {

		        	if ($passwordError) {
	        			$passwordError = $passwordError.'<br />You forgot to enter a new password. Please try again.';
	        			$hasError = true;
	        		}
	        		else {
	        			$passwordError = 'You forgot to enter a new password. Please try again.';
			        	$hasError = true;
	        		}
		        }


		        if ( $_POST['current_password'] != "" && isset($_POST['new_password']) && isset($_POST['password_confirm']) ) {

		        	
	        		// check current password first

		        	$user_info = get_userdata($current_user->ID);
      				$user_pass = $user_info->user_pass;


					$plain_password = $_POST['current_password'];

					if ( wp_check_password( $_POST['current_password'], $user_pass, $current_user->ID) ) {
					    
			        	// check the new password and confirm match
			        	if ( $_POST['new_password'] != $_POST['password_confirm'] ) {
				        	$passwordError = 'Your passwords do not match. Please try again.';
				        	$hasError = true;
				        }

				        else {
				        	$new_password = $_POST['new_password'];
				        	wp_set_password( $new_password, $current_user->ID );

				        	$passwordSavedAlert = 'Your password has successfully been updated!';
				        }

					} 
					
					else {
					    $currentPasswordError = 'Your current password was not correct. Please try again.';
			        	$hasError = true;
					}
		        

		        }

		        

		        if ( !isset($hasError) ) {
		        	wp_update_user( array( 
							'ID' => $current_user->ID, 
							'user_email' => wp_strip_all_tags($user_email),
							) 
					);
		        	if (isset($_POST['user_phone'])) {
		        		$phoneNumeric = preg_replace('/\D/', '', $_POST['user_phone']);
						update_user_meta( $current_user->ID, 'user_phone', wp_strip_all_tags($phoneNumeric));
		        	}
		        	
		        	if (isset($_POST['user_zip'])) {
						$zipNumeric = preg_replace('/\D/', '', $_POST['user_zip']);
						update_user_meta( $current_user->ID, 'user_zip', wp_strip_all_tags($zipNumeric));
					}

					if (isset($_POST['sms_alert'])) {
						update_user_meta( $current_user->ID, 'sms_alert', wp_strip_all_tags($_POST['sms_alert']));
					}
					
					if (isset($_POST['email_alert'])) {
						update_user_meta( $current_user->ID, 'email_alert', wp_strip_all_tags($_POST['email_alert']));
					}

					if (isset($_POST['alert_radius'])) {
						update_user_meta( $current_user->ID, 'alert_radius', wp_strip_all_tags($_POST['alert_radius']));
					}

					$zip = get_the_author_meta('user_zip', $current_user->ID);
					$sms_alert = get_the_author_meta('sms_alert', $current_user->ID);
					$email_alert = get_the_author_meta('email_alert', $current_user->ID);
					$alert_radius = get_the_author_meta('alert_radius', $current_user->ID);
					

					$email = get_the_author_meta('user_email', $current_user->ID);
					$phone = get_the_author_meta('user_phone', $current_user->ID);
					$zip = get_the_author_meta('user_zip', $current_user->ID);
					$sms_alert = get_the_author_meta('sms_alert', $current_user->ID);
					$email_alert = get_the_author_meta('email_alert', $current_user->ID);
					$alert_radius = get_the_author_meta('alert_radius', $current_user->ID);
					

					$savedAlert = 'Your settings have successfully been updated!';
		        }
				
			}

		}
		else {
			$nonceError = 'Invalid nonce.';
      		$hasError = true;
		}
	}

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
			<?php if($trainerAlertsMessage) {
				?>
				<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>We have you registered as a trainer. If you would like to receive alerts, please contact us at <a href="mailto:service@thinbluetraining.com">service@thinbluetraining.com</a>.
                </div>
                <?php
			}
			?>
			<h1><?php if ($is_trainer) {echo 'Trainer ';} else {echo 'Member ';} ?>Account Settings</h1>
			<?php if(isset($hasError) ) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>There was an error updating your settings.
                    </div>
                <?php } ?>
                <?php if(isset($savedAlert) ) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $savedAlert; ?>
                    </div>
                <?php } ?>
                <?php if(isset($passwordSavedAlert) ) { ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $passwordSavedAlert; ?>
                    </div>
                <?php } ?>

                <?php 
					if ( $displayTools ) {
					?>
					<div class="btn-group" style="float: right; margin-bottom: 20px;">
				  		<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#"><?php if ($is_trainer) {echo 'Trainer ';} else { echo 'Member '; } ?>Tools&nbsp;<span class="caret"></span></button>
				  		<ul class="dropdown-menu" style="margin-left: -92px; text-align: left;">
				  			<?php if ($is_trainer) {
				  					?>
				  					<li><a href="<?php bloginfo('url'); ?>/post-class">Add New Class</a></li>
				  					<li><a href="<?php bloginfo('url'); ?>/view-listings">Edit Existing Classes</a></li>
				  					<?php }
				  			?>
				    		<li><a href="<?php bloginfo('url'); ?>/post-job">Add New Job</a></li>
				  		</ul>
					</div>
					<?php 
				} ?>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="clear: both;">

				<?php if ($is_trainer) {
				?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="classListingSettingsHeading">
						
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#classListingSettings" aria-expanded="true" aria-controls="classListingSettings">
  							<h4 class="panel-title">Options for Trainers<i class="fa fa-plus" style="float: right; display: none;"></i><i class="fa fa-minus" style="float: right;"></i></h4>
						</a>
						
					</div>
					<div id="classListingSettings" class="panel-collapse collapse <?php if ($is_trainer) {echo 'in';} ?>" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
						    
	    					<div class="row">
								<div class="col-xs-6" style="text-align: center;">
									<a class="btn btn-lg btn-primary" href="<?php bloginfo('url'); ?>/post-class">Post a class</a>
								</div>
								<div class="col-xs-6" style="text-align: center;">
									<a class="btn btn-lg btn-primary" href="<?php bloginfo('url'); ?>/view-listings">Edit existing classes</a>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<?php } ?>

				<?php if ($is_recruiter) {
				?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="classListingSettingsHeading">
						
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#classListingSettings" aria-expanded="true" aria-controls="classListingSettings">
  							<h4 class="panel-title">Options for Recruiters<i class="fa fa-plus" style="float: right; display: none;"></i><i class="fa fa-minus" style="float: right;"></i></h4>
						</a>
						
					</div>
					<div id="classListingSettings" class="panel-collapse collapse <?php if ($is_recruiter) {echo 'in';} ?>" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
						        <div class="well well-lg" style="text-align: center; margin-bottom: 20px; margin-top: 20px;">
		    						<div class="row">
										<div class="col-xs-12">
											<a style="color: #FFFFFF;" href="<?php bloginfo('url'); ?>/post-job" class="btn btn-primary btn-lg">Post a job now!</a>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
				<?php } ?>


  				<div class="panel panel-default">
    				<div class="panel-heading" role="tab" id="alertSettingsHeading">
      					
    					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#alertSettings" aria-expanded="false" aria-controls="alertSettings">
      						<h4 class="panel-title">Account <?php if (!$is_trainer && !$is_recruiter) {echo '& Alert ';} ?>Settings <i class="fa fa-plus" style="float: right; <?php if (!$is_trainer || !$is_recruiter) {echo 'display: none;';} ?>"></i><i class="fa fa-minus" style="float: right; <?php if ($is_trainer || $is_recruiter) {echo 'display: none;';} ?>"></i></h4>
    					</a>
      					
    				</div>
	    			<div id="alertSettings" class="panel-collapse collapse <?php if (!$is_trainer) {echo 'in';} ?>" role="tabpanel" aria-labelledby="headingOne">
	      				<div class="panel-body">
	      					<?php if ($is_trainer) {
				  					?>
	      							<p style="font-style: italic;">Use the fields below to change your password.</p>
	      					<?php } 
	      						else {
	      							?>
	      							<p style="font-style: italic;">Use the fields below to set up your alert settings or change your password.</p>
	      							<?php
	      						}
	      						?>
					        <form action="" method="post" class="member-settings form-horizontal">
			  					<fieldset>
			  						<?php if (!$is_trainer) {
				  					?>
			    					<legend>Alert Settings</legend>

			    					<?php if ($is_recruiter) {
			    						echo '<p>As a recruiter, you can sign up to have alerts for class listings. If you don\'t wish to receive alerts, just leave these fields blank.</p>';
			    					}
			    					?>
			    					
				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Phone Number</label>
				    					<div class="col-sm-10">
					    					<input class="form-control" type="text" name="user_phone" value="<?php echo $phone; ?>" />
					    					<span class="help-block">We’ll use this number to send you text message alerts when a new class is listed in your area. (Your normal SMS rates will apply)</span>
					    				</div>
				    				</div>

				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Zipcode</label>
				    					<div class="col-sm-10">
					    					<input class="form-control" type="text" name="user_zip" value="<?php echo $zip; ?>" />
					    					<span class="help-block">We’ll use your zip code to determine which events are close to you</span>
					    				</div>
				    				</div>

				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Receive Text Alerts</label>
				    					<div class="col-sm-10">
				    						<input type="checkbox" name="sms_alert" id="sms_alert" <?php if( $sms_alert == 'on' ) { ?>checked="checked"<?php } ?> class="form-control" />
					    					<span class="help-block">Check this box if you’d like to receive SMS text messages whenever a new class is listed in your area.</span>
					    				</div>
				    				</div>

				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Receive Email Alerts</label>
				    					<div class="col-sm-10">
				    						<input type="checkbox" name="email_alert" id="email_alert" <?php if( $email_alert == 'on' ) { ?>checked="checked"<?php } ?> class="form-control" />
					    					<span class="help-block">Check this box if you’d like to receive email alerts whenever a new class is listed in your area.</span>
					    				</div>
				    				</div>

				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Alert Radius</label>
				    					<div class="col-sm-10">
					    					<select name="alert_radius" id="alert_radius" class="form-control">
												<option value="">--</option>
										        <option value="25" <?php if ( $alert_radius == 25 ) { echo "selected='selected'"; } ?>>25 miles</option>
										        <option value="50" <?php if ( $alert_radius == 50 ) { echo "selected='selected'"; } ?>>50 miles</option>
										        <option value="100" <?php if ( $alert_radius == 100 ) { echo "selected='selected'"; } ?>>100 miles</option>
										        <option value="250" <?php if ( $alert_radius == 250 ) { echo "selected='selected'"; } ?>>250 miles</option>
										        <option value="500" <?php if ( $alert_radius == 500 ) { echo "selected='selected'"; } ?>>500 miles</option>
										    </select>
					    					<span class="help-block">Select the area you want to be alerted about. If a new class is listed within that area, we’ll let you know!</span>
					    				</div>
				    				</div>
				    				<?php } ?>
				    				<legend>Change your password</legend>


				    				<?php 
				    					if(isset($currentPasswordError) && $currentPasswordError != '') { ?>
				                         	<div class="alert alert-danger col-sm-12"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $currentPasswordError;?></div>
				                      	<?php } 
				                    ?>
				    				<div class="form-group <?php if(isset($currentPasswordError) && $currentPasswordError != '') { echo 'has-error'; } ?>">
				    					<label class="col-sm-2 control-label">Current Password</label>
				                      	<div class="col-sm-10">
				    						<input class="form-control" type="password" placeholder="Current Password" name="current_password" >
				    					</div>
				    				</div>
				    				<?php 
				    					if(isset($passwordError) && $passwordError != '') { ?>
				                         	<div class="alert alert-danger col-sm-12"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $passwordError;?></div>
				                      	<?php } 
				                    ?>
				    				<div class="form-group <?php if(isset($passwordError) && $passwordError != '') { echo 'has-error'; } ?>">
				    					<label class="col-sm-2 control-label">New Password</label>
				                      	<div class="col-sm-10">
				    						<input class="form-control" type="password" placeholder="New Password" name="new_password" >
				    					</div>
				    				</div>
				    				<div class="form-group">
				    					<label class="col-sm-2 control-label">Confirm Password</label>
				    					<div class="col-sm-10">
				    						<input class="form-control" type="password" placeholder="Confirm Password" name="password_confirm" >
				    					</div>
				    				</div>

				    				<legend>Change your e-mail address</legend>

				    				<?php 
				    					if(isset($emailError) && $emailError != '') { ?>
				                         	<div class="alert alert-danger col-sm-12"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $emailError;?></div>
				                      	<?php } 
				                    ?>
				    				<div class="form-group <?php if(isset($emailError) && $emailError != '') { echo 'has-error'; } ?>">
				    					<label class="col-sm-2 control-label">E-Mail</label>
				                      	<div class="col-sm-10">
				    						<input class="form-control" type="text" placeholder="E-mail" name="user_email" value="<?php echo $email; ?>" />
				    					</div>
				    				</div>
			    					
			    					<input type="hidden" name="username" value="<?php echo $current_user->user_login; ?>" />
			    					<input type="hidden" name="action" value="settings_posted" />
			    					<?php wp_nonce_field( 'memberSettings', 'settings_posted' ); ?>

			    					<div class="form-group">
			    						<div class="col-sm-offset-2 col-sm-10">
			    							<button type="submit" class="btn btn-primary btn-lg">Update</button>
			    						</div>
			    					</div>
			  					</fieldset>
							</form>
	      				</div>
	    			</div>

	    			
	  			</div>
	  		</div>
			

		</div>

		<?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>