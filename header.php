<?php
/**
 * header.php
 *
 * Includes <head> section, start of <body> and primary navigation
 *
 * @package bootstrapped
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('title'); ?></title>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/favicon.ico" type="image/x-icon"/>
		<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/apple-touch-icon.png">
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic|Source+Sans+Pro:400,400italic,300italic,300,600,600italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>
		<!-- html5.js -->
			<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->	
			
				<!-- respond.js -->
			<!--[if lt IE 9]>
			          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
			<![endif]-->	
	</head>

	<body <?php body_class(); ?>>
		<div id="wrap">

			<?php 
				// check if a user is logged in
				if (is_user_logged_in()) {
					$user = wp_get_current_user();
					$logged_in = true;

					$user_info = get_userdata($user->ID);

					if ( in_array('trainer', $user_info->roles) ) {
						$is_trainer = true;
					}
					else {
						$is_trainer = false;
					}
				}
				else {
					$logged_in = false;
					$is_trainer = false;
				}


				

			?>
			<!-- account access tab -->
			<div id="account-tab">
				<?php if ($logged_in) {
					echo $user->display_name."&nbsp;"; } ?><i class="fa fa-user"></i>&nbsp;<i class="fa fa-angle-down"></i><i class="fa fa-angle-up" style="display: none;"></i>
			</div>
			<div id="account-tab-list">
				<?php 
					if(!$logged_in) {
						?>
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/login">Login</a></li>
							<li><a href="<?php bloginfo('url'); ?>/registration">Register</a></li>
						</ul>
						<?php
					}
					else {
						?>
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/member-settings">Account Settings</a></li>
							<?php if ($is_trainer) {
								?>
								<li><a href="<?php bloginfo('url'); ?>/post-class">Post a class</a></li>
							<?php } ?>
							<li><a href="<?php bloginfo('url'); ?>/post-job">Post a job</a></li>
							<li><a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></li>
						</ul>
						<?php
					}
				?>
				
			</div>

			<!-- navigation -->
			<nav id="primary-nav" class="navbar navbar-inverse" role="navigation">
	  			<div class="container">
	    			<div class="navbar-header">
	      				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-nav-collapse">
		        			<span class="sr-only">Toggle navigation</span>
					        <i class="fa fa-navicon" style="color: white; font-size: 24px;"></i>
	      				</button>
	      				<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
	      					<img src="<?php bloginfo('stylesheet_directory'); ?>/dist/img/logo-large.png" style="max-height: 100px;" />
	      				</a>
	    			</div>
	    			<div class="collapse navbar-collapse" id="primary-nav-collapse">
	    				<?php
	    					$calendar_active;
	    					$jobs_active;
	    					$podcast_active;
	    					$resources_active;
	    					$about_active;

	    					if (is_page('about')) {
	    						$about_active = 'active';
	    					}

	    					if (is_page('podcast')) {
	    						$podcast_active = 'active';
	    					}

	    					if (is_page('resources')) {
	    						$resources_active = 'active';
	    					}

	    					if (is_post_type_archive('event') || is_home() ) {
	    						$calendar_active = 'active';
	    					}

	    					if (is_post_type_archive('job')) {
	    						$jobs_active = 'active';
	    					}

	    					if (is_page('contact')) {
	    						$contact_active = 'active';
	    					}

	    				?>
	      				<ul class="nav navbar-nav navbar-right">
					        <li><a class="<?php echo $calendar_active; ?>" href="<?php bloginfo('url'); ?>/calendar">Calendar<span class="nav-sub-text">Find Your Next Class</span></a></li>
					        <li><a class="<?php echo $jobs_active; ?>" href="<?php bloginfo('url'); ?>/jobs">Jobs<span class="nav-sub-text">See Who’s Hiring</span></a></li>
					        <li><a class="<?php echo $podcast_active; ?>" href="<?php bloginfo('url'); ?>/podcast">Podcast<span class="nav-sub-text">iTunes Broadcast</span></a></li>
					        <li><a class="<?php echo $resources_active; ?>" href="<?php bloginfo('url'); ?>/resources">Resources<span class="nav-sub-text">Gear and Intel</span></a></li>
					        <li><a class="<?php echo $about_active; ?>" href="<?php bloginfo('url'); ?>/about">About Us<span class="nav-sub-text">Our Background</span></a></li>
					        <li><a class="<?php echo $contact_active; ?>" href="<?php bloginfo('url'); ?>/contact">Contact<span class="nav-sub-text">How Can We Help?</span></a></li>
					        <li id="account-tab-list-mobile">
					        	<?php if ($logged_in) {
									echo $user->display_name."&nbsp;"; } ?><i class="fa fa-user"></i>&nbsp;<i class="fa fa-angle-down"></i><i class="fa fa-angle-up" style="display: none;"></i>
					        	<?php 
									if(!$logged_in) {
										?>
										<ul>
											<li><a href="<?php bloginfo('url'); ?>/login">Login</a></li>
											<li><a href="<?php bloginfo('url'); ?>/registration">Register</a></li>
										</ul>
										<?php
									}
									else {
										?>
										<ul>
											<li><a href="<?php bloginfo('url'); ?>/member-settings">Account Settings</a></li>
											<?php if ($is_trainer) {
												?>
												<li><a href="<?php bloginfo('url'); ?>/post-class">Post a class</a></li>
											<?php } ?>
											<li><a href="<?php bloginfo('url'); ?>/post-job">Post a job</a></li>
											<li><a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a></li>
										</ul>
										<?php
									}
								?>
							</li>
	      				</ul>
	    			</div><!-- /.navbar-collapse -->
	  			</div><!-- /.container-fluid -->
			</nav>	
			<!-- start content area -->
			<div id="main-content" class="container">	