<?php
/**
 * page-twilio-test.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php
	          	// get zip codes within 25, 50 and 100 mile radius 
          	$zip = 21202;
    $zips_25 = [];
    $zips_50 = [];
    $zips_100 = [];

    $zips_25_json = zipRadius($zip, 25);
    foreach($zips_25_json->zip_codes as $zipcode) {
        array_push($zips_25, $zipcode->zip_code);
    }

    $zips_50_json = zipRadius($zip, 50);
    foreach($zips_50_json->zip_codes as $zipcode) {
        array_push($zips_50, $zipcode->zip_code);
    }

    $zips_100_json = zipRadius($zip, 100);
    foreach($zips_100_json->zip_codes as $zipcode) {
        array_push($zips_100, $zipcode->zip_code);
    }

    // get the users by radius preference
    $users_25 = [];
    $users_50 = [];
    $users_100 = [];

    $args25 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 25,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_25_query = new WP_User_Query($args25);
    $users_25 = $users_25_query->get_results();

    $args50 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 50,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_50_query = new WP_User_Query($args50);
    $users_50 = $users_50_query->get_results();

    $args100 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 100,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_100_query = new WP_User_Query($args100);
    $users_100 = $users_100_query->get_results();

    $targeted_members = [];

    foreach ($users_25 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_25 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_50 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_50 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_100 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_100 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    // loop through the targeted members by ID to get their phone numbers and names
    $members = [];
    foreach($targeted_members as $member) {
    	$member_phone = get_the_author_meta( 'user_phone', $member );
    	$member_name = get_the_author_meta( 'first_name', $member ) . " " . get_the_author_meta( 'last_name', $member );
    	$members[$member_phone] = $member_name;
    	print_r($members);
    	echo '<br />';
    }
    //print_r($members);
    //sendSMS($members, 131);

// 	$emails = [];
// 		foreach ($targeted_members as $user) {
// 	    	if ( get_the_author_meta( 'user_email', $user ) ) {
// 	    		$address = get_the_author_meta( 'user_email', $user );
// 	    		array_push( $emails, $address );
// 	    	}
// 	    }

// print_r($emails);
 //          	$name = 'Will Spencer';
 //          	$link = get_permalink(131);
 //          	$email = get_the_author_meta( 'user_email', 1 );
 //          	$subject = 'Training Alert - Thin Blue Training';
	// $body = 'Hey $name, a new training event has been posted close to you. Click the following link for details! '.$link;
	// $headers = 'From: Thin Blue Training <contact@thinbluetraining.com>' . "\r\n";

	// wp_mail( $email, $subject, $body, $headers );
	// echo $email;
			?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>