<?php
/**
 * page-success.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?>!</h1>   
        	<?php endwhile; ?>

        	<?php 

        		if (isset($_GET['couponCode'])) {
            		// the user had a coupon code for a free listing
            		$isFreeListing = true;

            		$couponCode = strtoupper($_GET['couponCode']);

					// check if coupon code exists
					$coupon = get_posts('name='.$couponCode.'&post_type=coupon');
					
					if (!empty($coupon)) {
						$coupon_id = $coupon[0]->ID;

						// check if coupon quantity is sufficient
						$quantity = get_post_meta( $coupon_id, '_wsdev_coupon_quantity', true);

						if ($quantity != '') {
							// there is a set quantity, so decrease the quantity by one
							$newQuantity = $quantity - 1;
							update_post_meta( $coupon_id, '_wsdev_coupon_quantity', $newQuantity);
							
						}

					}
					// get the current user
            		$current_user = wp_get_current_user();
					// add the updated listings amount to the database
					$available_listings = get_the_author_meta('_wsdev_listings', $current_user->ID);
					$available_listings = $available_listings + 1;
         			update_user_meta( $current_user->ID, '_wsdev_listings', $available_listings);

					$message = '<h3>Congratulations! Your coupon code has given you one free listing.</h3>';

            	}


	            if ( isset($_GET) && !isset($_GET['couponCode']) ) {

	            	$transaction = $_GET;

	            	$trans_id = $transaction['tx'];
	            	$trans_status = $transaction['st'];
	            	$trans_amount = $transaction['amt'];

	            	$trans_quantity = explode(',', $transaction['cm']);
	            	$listing_quantity = $trans_quantity[0];
	            	$bold_quantity = $trans_quantity[1];
	            	$coupon_used = $trans_quantity[2];

	            

		            // get the current user
		            $current_user = wp_get_current_user();

		         	// check if transaction is successful
		         	if ( $trans_status != 'Completed') {
		         		// redirect back to purchase listing page
		         		//header('Location: ' . get_permalink(187));
		         	}

		         	else {
		         		// check if transaction ID already exists in the database to prevent double submission
		         		$user_transactions = get_the_author_meta('_wsdev_transactions', $current_user->ID);

		         		$duplicate_trans = false;

		         		if (is_array($user_transactions)) {
			         		foreach ($user_transactions as $trans) {
			         			if (in_array($trans_id, $trans)) {
			         				$duplicate_trans = true;
			         			}
			         		}
			         	}

		         		// if the transaction doesn't exist, this is a NEW transaction
		         		if (!$duplicate_trans) {

		         			// add this transaction to the saved transaction array
		         			if (is_array($user_transactions)) {
		         				array_push($user_transactions, $transaction);
		         			}
		         			else {
		         				$user_transactions = array($transaction);
		         			}

		         			// add the updated transactions array to the database
		         			update_user_meta( $current_user->ID, '_wsdev_transactions', $user_transactions);

		         			// get any remaining listings from the database
		         			$available_listings = get_the_author_meta('_wsdev_listings', $current_user->ID);
		         			$available_listings = $available_listings + $listing_quantity;

		         			// add the updated listings amount to the database
		         			update_user_meta( $current_user->ID, '_wsdev_listings', $available_listings);

		         			// get any remaining BOLD listings from the database
		         			$available_bold_listings = get_the_author_meta('_wsdev_bold_listings', $current_user->ID);
		         			$available_bold_listings = $available_bold_listings + $bold_quantity;

		         			// add the updated BOLD listings amount to the database
		         			update_user_meta( $current_user->ID, '_wsdev_bold_listings', $available_bold_listings);

		         			// get the total purchase amount from the database
		         			$total_purchases = get_the_author_meta('_wsdev_total_purchases', $current_user->ID);
		         			$total_purchases = $total_purchases + $trans_amount;

		         			// add the updated purchase amount to the database
		         			update_user_meta( $current_user->ID, '_wsdev_total_purchases', $total_purchases);

		         		}

		         		// update the coupon code quantity, if it exists
		         		$couponCode = strtoupper($_GET['couponCode']);

						// check if coupon code exists
						$coupon = get_posts('name='.$couponCode.'&post_type=coupon');
						$coupon_id = $coupon[0]->ID;

						$quantity = get_post_meta( $coupon_id, '_wsdev_coupon_quantity', true);

						if ($quantity == '') {
							// there is no set quantity so we don't have to update it
							return;
						}

						else {
							$updated_quantity = $quantity - 1;
							update_post_meta($coupon_id, '_wsdev_coupon_quantity', $updated_quantity);
						}
		         	}
		         }
	            	 
	        ?>

	        <?php if($message && $message != '') {
	        	echo $message;
	        }
	        else {
	        	?>
		        <h3>Thank you for your purchase of <?php echo $listing_quantity; ?> class listing<?php if ($trans_quantity > 1) { echo 's'; }; ?><?php if($bold_quantity > 0) { echo ' and '.$bold_quantity.' bold listing'; }; if ($bold_quantity > 1) { echo 's'; }?>!</h3>
		        <?php
		        } ?>
	        <p>You may now post your class by <a href="<?php bloginfo('url'); ?>/post-class">clicking here.</a></p>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>