<?php
/**
 * 404.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<h1>Woops!</h1>
          	<p>The page you're looking for wasn't able to be found. Please use the navigation links above or search for the page you're looking for using the form below.</p>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>