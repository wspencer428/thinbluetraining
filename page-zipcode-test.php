<?php
/**
 * page-zipcode-test.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
			<div class="well">
				<h3>Zip code by distance</h3>
	          	Zipcode 1: <input id="zipcode1" type="text" /><br />
	          	Zipcode 2: <input id="zipcode2" type="text" /><br />
	          	<button id="zipSubmit">Submit</button><br />
	          	<p id="returnText"></p>
	        </div>
	        <br />
	        <div class="well">
	        	<h3>Zip codes within 25 miles of entered zipcode</h3>
	        	Enter zipcode: <input id="zipTarget" type="text" /><br />
	        	<button id="zipRadiusSubmit">Check</button><br />
	        	<p id="returnRadiusText"></p>
	        </div>



		<script>
			jQuery('#zipSubmit').click(function() {
				zipDistanceCheck();
			});

			jQuery('#zipRadiusSubmit').click(function() {
				zipRadiusCheck();
			});

			function zipDistanceCheck() {

				var zipcode1 = jQuery('#zipcode1').val();
				var zipcode2 = jQuery('#zipcode2').val();
				var response;

				jQuery.ajax({
					type: 'GET',
					url: '<?php bloginfo("url"); ?>/wp-content/themes/bootstrapped/zip-distance.php',
					data: "zipcode1="+zipcode1+"&zipcode2="+zipcode2,
					success: function(fetchResult) {
						jQuery('#returnText').html(fetchResult);
						
					},
					error: function() {
						alert('fetch failed');
					}
				});
				
			};

			function zipRadiusCheck() {

				var zipTarget = jQuery('#zipTarget').val();
				var response;
				var responseArray = [];

				jQuery.ajax({
					type: 'GET',
					url: '<?php bloginfo("url"); ?>/wp-content/themes/bootstrapped/zip-radius.php',
					data: "zipTarget="+zipTarget,
					dataType: 'json',
					success: function(fetchResult) {
						if (fetchResult['error_code']) {
							jQuery('#returnRadiusText').html(fetchResult['error_msg']);
						}
						else {
							jQuery('#returnRadiusText').html('');
							for (i = 0; i < fetchResult['zip_codes'].length; i++) { 
							    responseArray.push(fetchResult['zip_codes'][i]['zip_code']);
							}
							for (x = 0; x < responseArray.length; x++) {
								jQuery('#returnRadiusText').append(responseArray[x] + ', ');
							}
						}
						
					},
					error: function() {
						alert('fetch failed');
					}
				});
				
			};
		</script>

        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>