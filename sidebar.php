<?php
/**
 * sidebar.php
 *
 * @package bootstrapped
 */
?>

<?php if (is_user_logged_in()) {
					
		$current_user = wp_get_current_user();

		$user_info = get_userdata($current_user->ID);

		if ( in_array('trainer', $user_info->roles) || get_the_author_meta('user_type', $current_user->ID) == 'trainer' ) {
			$linkPath = 'member-settings?from=alerts';
		}
		else {
			$linkPath = 'member-settings';
		}
		
	}
	else {
		$linkPath = 'registration';
	}
?>
<div id="main-sidebar" class="col-md-4 col-md-offset-1">

	<div class="row">
		<div class="col-xs-12" style="text-align: center;">
			<a href="<?php bloginfo('url'); echo '/'.$linkPath; ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/dist/img/sign-up7.png" class="img-responsive img-thumbnail" /></a>
			<h4>Sign up to receive <strong>FREE</strong> text alerts when a new training event is posted within range of your zip code.</h4>
		</div>
	</div>

	<!-- <div class="row">
		<div class="col-xs-12" style="text-align: center;">
			<h3 style="text-decoration: underline;">Latest Podcast Episode</h3>
			<?php echo do_shortcode('[smart_track_player url="http://traffic.libsyn.com/thinbluetraining/TBT5.mp3" title="TBT Podcast" download="false" social="true" social_twitter="true" social_facebook="true" social_gplus="true" ]');?>
		</div>
	</div> -->
	<div class="row">
		<div class="col-xs-12" style="text-align: center; margin-top: 25px; margin-bottom: 25px;">
			<a href="<?php bloginfo('url'); ?>/podcast" class="btn btn-primary btn-lg">Listen to the Latest Podcast</a>
		</div>
	</div>
</div>