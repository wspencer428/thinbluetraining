<?php
/**
 * page-trainer-login.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            
	            <?php the_content(); ?>
        	<?php endwhile; ?>
        </div>
        <input type="hidden" id="blogURL" value="<?php bloginfo('url'); ?>" />
        <script>

        	var blogURL = jQuery('#blogURL').val();
        	jQuery(document).ready(function() {
        		jQuery('#login a[href$="registration/"]').attr('href', blogURL+'/trainer-registration/');
        	});
        	

        </script>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>