<?php
/**
 * archive-job.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	
	        <h1>Jobs</h1>
	        <!-- <div class="alert alert-info" role="alert">Is your agency hiring? Post your job opening on Thin Blue Training by <a href="<?php bloginfo('url'); ?>/post-job">clicking here!</a></div> -->
	        <?php include(locate_template('partials/listing-jobs.php')); ?>
	        
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>