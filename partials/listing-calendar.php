<?php

	$search_term = '';
	if (isset($_GET['state'])) {
		$state = $_GET['state'];
		$search_term = 'state';
		if ($state == 'ALL') {
			$state = '';
		}
	}
	else {
		$state = '';
	}

	if(isset($_GET['topic'])) {
		$topic = $_GET['topic'];
		$search_term = 'topic';
		if ($topic == 'ALL') {
			$topic = '';
		}
	}
	else {
		$topic = '';
	}

	if(isset($_GET['month'])) {
		$month = $_GET['month'];
		$search_term = 'month';
		if ($month == 'ALL') {
			$month = '';
		}
	}
	else {
		$month = '';
	}

	if (isset($_GET['zip'])) {
		$zip = $_GET['zip'];
		$search_term = 'zip';
	}

	else {
		$zip = '';
	}

	if (isset($_GET['radius'])) {
		$radius = $_GET['radius'];
		$search_term = 'zip';
	}

	else {
		$radius = '';
	}

	
?>

<a href="<?php bloginfo('url'); ?>/post-class" class="trainer-click-here" style="position: absolute;"><img src="<?php bloginfo('stylesheet_directory'); ?>/dist/img/post-class-free.png" class="img-responsive"/></a>

<form id="topic_form" class="form-inline pull-right" style="text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/calendar/" method="GET">
	<div class="form-group">
		<label for="state">Search by Topic: </label>
		<select class="form-control" id="topic_select" name="topic" style="margin-top: 5px; width: 155px;">
			<option value="">Select a topic</option>
			<option value="ALL">All Topics</option>
			<option value="Accident Investigation">Accident Investigation</option>
			<option value="Accident Reconstruction">Accident Reconstruction</option>
			<option value="Active Shooter">Active Shooter</option>
			<option value="Administration">Administration</option>
			<option value="Aquatic Investigations">Aquatic Investigations</option>
			<option value="Ambush">Ambush</option>
			<option value="Armorer">Armorer</option>
			<option value="Arrest &amp; Control">Arrest &amp; Control</option>
			<option value="Arson">Arson</option>
			<option value="Asset Forfeiture">Asset Forfeiture</option>
			<option value="Assault">Assault</option>
			<option value="Autism">Autism</option>
			<option value="Auto Theft">Auto Theft</option>
			<option value="Background Investigation">Background Investigation</option>
			<option value="Bicycle Accidents">Bicycle Accidents</option>
			<option value="Bicycle Operations">Bicycle Operations</option>
			<option value="Body Worn Cameras">Body Worn Cameras</option>
			<option value="Breaching">Breaching</option>
			<option value="Budgeting">Budgeting</option>
			<option value="Burglary">Burglary</option>
			<option value="Cameras- Body Worn">Cameras- Body Worn</option>
			<option value="Canine">Canine</option>
			<option value="Cartel">Cartel</option>
			<option value="Case Management">Case Management</option>
			<option value="Casualty Training">Casualty Training</option>
			<option value="Cellphone Investigations">Cellphone Investigations</option>
			<option value="Chemical Aerosol">Chemical Aerosol</option>
			<option value="Child Abuse">Child Abuse</option>
			<option value="Child Death">Child Death</option>
			<option value="Child Exploitation">Child Exploitation</option>
			<option value="Child Porn">Child Porn</option>
			<option value="Citizen Academies">Citizen Academies</option>
			<option value="College &amp; University">College &amp; University</option>
			<option value="Communications">Communications</option>
			<option value="Community Policing">Community Policing</option>
			<option value="Computer Crimes">Computer Crimes</option>
			<option value="Computer Forensics">Computer Forensics</option>
			<option value="Concealment">Concealment</option>
			<option value="Conference &amp; Events">Conference &amp; Events</option>
			<option value="Confidential Informants">Confidential Informants</option>
			<option value="Conflict Resolution">Conflict Resolution</option>
			<option value="Confrontation Avoidance">Confrontation Avoidance</option>
			<option value="Conservation Law Enforcement">Conservation Law Enforcement</option>
			<option value="Corrections">Corrections</option>
			<option value="Courthouse">Courthouse </option>
			<option value="Courtroom Testimony">Courtroom Testimony</option>
			<option value="Craigslist">Craigslist</option>
			<option value="Critical Incident Command">Critical Incident Command</option>
			<option value="Crime Prevention">Crime Prevention</option>
			<option value="Crime Scene Investigation">Crime Scene Investigation</option>
			<option value="Crisis &amp; Hostage">Crisis &amp; Hostage</option>
			<option value="Custodial Death">Custodial Death</option>
			<option value="Cyberbullying">Cyberbullying</option>
			<option value="Deaf &amp; Hard of Hearing">Deaf &amp; Hard of Hearing</option>
			<option value="Deescalation">Deescalation </option>
			<option value="Defensive Tactics">Defensive Tactics</option>
			<option value="Detective Workshop">Detective Workshop</option>
			<option value="Dispatcher">Dispatcher</option>
			<option value="Disruptive Students">Disruptive Students</option>
			<option value="Domestic Violence">Domestic Violence</option>
			<option value="Drug Diversion">Drug Diversion</option>
			<option value="Drugs">Drugs</option>
			<option value="DUI">DUI</option>
			<option value="DWI">DWI</option>
			<option value="Electronic Control Device">Electronic Control Device</option>
			<option value="Electronic Countermeasures">Electronic Countermeasures</option>
			<option value="Emergency Medicine">Emergency Medicine</option>
			<option value="EMT">EMT</option>
			<option value="Entry">Entry</option>
			<option value="Evaluation">Evaluation</option>
			<option value="Evidence">Evidence</option>
			<option value="Executive Protection">Executive Protection</option>
			<option value="Expandable Baton">Expandable Baton</option>
			<option value="Explosives">Explosives</option>
			<option value="Explosive Breaching">Explosive Breaching</option>
			<option value="Facebook">Facebook</option>
			<option value="Fatality Accident Investigation">Fatality Accident Investigation</option>
			<option value="Field Training Officer">Field Training Officer</option>
			<option value="Financial">Financial</option>
			<option value="Fingerprinting">Fingerprinting</option>
			<option value="Firearms">Firearms</option>
			<option value="Fitness">Fitness</option>
			<option value="Foreign Languages">Foreign Languages</option>
			<option value="Forensics">Forensics</option>
			<option value="Fraud Investigations">Fraud Investigations</option>
			<option value="Gaming">Gaming</option>
			<option value="Gangs">Gangs</option>
			<option value="Google">Google</option>
			<option value="GPS">GPS</option>
			<option value="Grant Writing">Grant Writing</option>
			<option value="Ground Control">Ground Control</option>
			<option value="Handcuffing">Handcuffing</option>
			<option value="Handgun">Handgun</option>
			<option value="HazMat">HazMat</option>
			<option value="High Crime Areas">High Crime Areas</option>
			<option value="High Voltage Training">High Voltage Training</option>
			<option value="Homicide">Homicide </option>
			<option value="Hostage Negotiation">Hostage Negotiation</option>
			<option value="Immigration">Immigration</option>
			<option value="Infanticide">Infanticide</option>
			<option value="Injury Prevention">Injury Prevention</option>
			<option value="In-Service Training">In-Service Training</option>
			<option value="Instructor">Instructor</option>
			<option value="Intelligence">Intelligence</option>
			<option value="Interdiction">Interdiction</option>
			<option value="Internal Affairs">Internal Affairs</option>
			<option value="Internet Investigations">Internet Investigations</option>
			<option value="Internet Protocol Cameras">Internet Protocol Cameras</option>
			<option value="Interviews">Interviews</option>
			<option value="Investigations">Investigations</option>
			<option value="Jail Operations">Jail Operations</option>
			<option value="Juvenile Investigations">Juvenile Investigations</option>
			<option value="K-9">K-9</option>
			<option value="Knife Defense">Knife Defense</option>
			<option value="Langauges">Languages</option>
			<option value="Leadership">Leadership</option>
			<option value="Legal">Legal</option>
			<option value="Less Lethal">Less Lethal</option>
			<option value="Liability Management">Liability Management</option>
			<option value="Light Energy Applications">Light Energy Applications</option>
			<option value="Lock Picking">Lock Picking</option>
			<option value="Management">Management</option>
			<option value="Marine Operations">Marine Operations</option>
			<option value="Media Relations">Media Relations</option>
			<option value="Mental Illness">Mental Illness</option>
			<option value="Mentoring">Mentoring</option>
			<option value="Minority Relations">Minority Relations</option>
			<option value="Miscellaneous">Miscellaneous</option>
			<option value="Motorcycle Gangs">Motorcycle Gangs</option>
			<option value="Motorcycle Operations">Motorcycle Operatios</option>
			<option value="Neighborhood Watch">Neighborhood Watch</option>
			<option value="Officer Involved Shooting">Officer Involved Shooting</option>
			<option value="Officer Survival">Officer Survival</option>
			<option value="Open Source Investigations">Open Source Investigations</option>
			<option value="Organized Crime">Organized Crime</option>
			<option value="Pathology">Pathology</option>
			<option value="Patrol">Patrol</option>
			<option value="Patrol Rifle">Patrol Rifle</option>
			<option value="Pawn Shops">Pawn Shops</option>
			<option value="Peer Support">Peer Support</option>
			<option value="Pharmaceutical Investigations">Pharmaceutical Investigations</option>
			<option value="Photography">Photography</option>
			<option value="Pinterest">Pinterest</option>
			<option value="Pistol">Pistol</option>
			<option value="Policy &amp; Procedure">Policy &amp; Procedure</option>
			<option value="Polygraph">Polygraph</option>
			<option value="Powerpoint">Powerpoint</option>
			<option value="Precursors">Precursors</option>
			<option value="Promotional Testing">Promotional Testing</option>
			<option value="Property Room">Property Room</option>
			<option value="Public Information Officer">Public Information Officer</option>
			<option value="Public Speaking">Public Speaking</option>
			<option value="Recruitment">Recruitment</option>
			<option value="Redman">Redman</option>
			<option value="Report Writing">Report Writing</option>
			<option value="Rifle">Rifle</option>
			<option value="Riot Control">Riot Control</option>
			<option value="Robbery">Robbery</option>
			<option value="School Resource">School Resource</option>
			<option value="SCUBA">SCUBA</option>
			<option value="Search &amp; Seizure">Search &amp; Seizure</option>
			<option value="Sex Crimes">Sex Crimes</option>
			<option value="Sex Trafficking">Sex Trafficking </option>
			<option value="Shoothouse">Shoothouse</option>
			<option value="Shotgun">Shotgun</option>
			<option value="Sign Language">Sign Language</option>
			<option value="Sniper">Sniper</option>
			<option value="Social Media">Social Media</option>
			<option value="Sovereign Citizens">Sovereign Citizens</option>
			<option value="Specialized Entry Techniques">Specialized Entry Techniques</option>
			<option value="Special Operations">Special Operations</option>
			<option value="Street Crimes">Street Crimes</option>
			<option value="Street Survival">Street Survival</option>
			<option value="Subversive Groups">Subversive Groups</option>
			<option value="Supervision">Supervision</option>
			<option value="Suicide">Suicide</option>
			<option value="Suicide Prevention">Suicide Prevention</option>
			<option value="Surveillance">Surveillance</option>
			<option value="SWAT">SWAT</option>
			<option value="Tactical Medicine">Tactical Medicine</option>
			<option value="Tactics">Tactics</option>
			<option value="Technical Operations">Technical Operations</option>
			<option value="Terrorism">Terrorism</option>
			<option value="Threat Assessment">Threat Assessment</option>
			<option value="Tracking">Tracking</option>
			<option value="Traffic">Traffic</option>
			<option value="Train the Trainer">Train the Trainer</option>
			<option value="Twitter">Twitter</option>
			<option value="Undercover">Undercover</option>
			<option value="Use of Force">Use of Force</option>
			<option value="Vehicular Operations">Vehicular Operations</option>
			<option value="Verbal Conflict">Verbal Conflict</option>
			<option value="Vice">Vice</option>
			<option value="Victimology">Victimology</option>
			<option value="Violent Crimes">Violent Crimes</option>
			<option value="VIP Protection">VIP Protection</option>
			<option value="Warrant Service">Warrant Service</option>
			<option value="Water Survival">Water Survival</option>
			<option value="Women in Law Enforcement">Women in Law Enforcement</option>
		</select>
	</div>
</form>
<br />
<form id="state_form_calendar" class="form-inline pull-right" style="clear: both; text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/calendar/" method="GET">
	<div class="form-group">
		<label for="zip">Search by State: </label>
		<select class="form-control" id="state_select" name="state" style="margin-top: 5px; width: 155px;">
			<option value="">Select a state</option>
			<option value="ALL">All States</option>
			<option value="AL">AL</option>
			<option value="AR">AR</option>
			<option value="AZ">AZ</option>
			<option value="CA">CA</option>
			<option value="CO">CO</option>
			<option value="CT">CT</option>
			<option value="DC">DC</option>
			<option value="DE">DE</option>
			<option value="FL">FL</option>
			<option value="GA">GA</option>
			<option value="HI">HI</option>
			<option value="IA">IA</option>
			<option value="ID">ID</option>
			<option value="IL">IL</option>
			<option value="IN">IN</option>
			<option value="KS">KS</option>
			<option value="KY">KY</option>
			<option value="LA">LA</option>
			<option value="MA">MA</option>
			<option value="MD">MD</option>
			<option value="ME">ME</option>
			<option value="MI">MI</option>
			<option value="MN">MN</option>
			<option value="MO">MO</option>
			<option value="MS">MS</option>
			<option value="MT">MT</option>
			<option value="NC">NC</option>
			<option value="ND">ND</option>
			<option value="NE">NE</option>
			<option value="NH">NH</option>
			<option value="NJ">NJ</option>
			<option value="NM">NM</option>
			<option value="NV">NV</option>
			<option value="NY">NY</option>
			<option value="OH">OH</option>
			<option value="OK">OK</option>
			<option value="OR">OR</option>
			<option value="PA">PA</option>
			<option value="RI">RI</option>
			<option value="SC">SC</option>
			<option value="SD">SD</option>
			<option value="TN">TN</option>
			<option value="TX">TX</option>
			<option value="UT">UT</option>
			<option value="VA">VA</option>
			<option value="VT">VT</option>
			<option value="WA">WA</option>
			<option value="WI">WI</option>
			<option value="WV">WV</option>
			<option value="WY">WY</option>
		</select>
	</div>
</form> 
<br />
<form id="month_form" class="form-inline pull-right" style="clear: both; text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/calendar/" method="GET">
	<div class="form-group">
		<label for="zip">Search by month: </label>
		<select class="form-control" id="month_select" name="month" style="margin-top: 5px; width: 155px;">
			<option value="">Select a month</option>
			<option value="ALL">All Months</option>
			<option value="01">January</option>
			<option value="02">February</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		</select>
	</div>
</form> 
<br />
<form id="zip_form" class="form-inline pull-right" style="clear: both; text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/calendar/" method="GET">
	<div class="form-group">
	<label for="zip">Search by zip code radius: </label>

		<input type="text" length="5" class="form-control" placeholder="5 digit" name="zip" style="margin-top: 5px; width: 90px;" />
		<select class="form-control" id="radius_select" name="radius" style="margin-top: 5px; width: 90px;">
			<option value="">--</option>
			<option value="25">25 miles</option>
			<option value="50">50 miles</option>
			<option value="75">75 miles</option>
			<option value="100">100 miles</option>
			<option value="250">250 miles</option>
			<option value="500">500 miles</option>
		</select>
	</div>
	<br />
	<div class="form-group" style="clear: both;">
		<button class="btn btn-primary" id="searchButton" style="margin-top: 20px;">Search</button>
	</div>
</form> 

<br />
<br />
<br />
<div class="input-group" style="clear: both; margin-top: 20px;"> <span class="input-group-addon">Filter</span>

	<input id="filter" type="text" class="form-control" placeholder="Filter training by Name, Date or State">
</div>
<table id="listing-table-head" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th style="width: 20%;">Date</th>
			<th style="width: 50%;">Title</th>
			<th style="width: 30%;">Location</th>
		</tr>
	</thead>
	
    <?php include(locate_template('partials/loop-calendar.php')); ?>
   
</table>