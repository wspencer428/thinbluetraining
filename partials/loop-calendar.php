<?php

    // variables from listing-calendar.php
    global $search_term;
    global $state;
    global $topic;
    global $month;
    global $zip;
    global $radius;

    $matched_events = null;

	$current_date = date("m/d/Y");
	$current_date = strtotime($current_date);

	if ($search_term == 'month') {
        $init_args = array(
            'post_type' => 'event',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_wsdev_event_date',
                    'value' => $current_date,
                    'compare' => '>=',
                    ),
                ),
            'meta_key' => '_wsdev_event_date',
            'orderby' => 'meta_value',
            'order' => 'ASC'
        );
        $init_events = new WP_Query($init_args);
        // put all the post IDs into an array
        $post_ids = wp_list_pluck( $init_events->posts, 'ID' );

        $matched_events = [];
        foreach($post_ids as $post) {
            $event_date_string = gmdate("m/d/Y", get_post_meta(get_the_ID(), '_wsdev_event_date', true));
			$event_date = explode('/', $event_date_string);
			$event_month = $event_date[0];

			if ($month == '') {
				array_push($matched_events, $post);
			}
			else  {
				if ($month == $event_month) {
					array_push($matched_events, $post);
				}
			}
        }
        wp_reset_query();
        wp_reset_postdata();
    }

    if ($search_term == 'zip') {
        $init_args = array(
            'post_type' => 'event',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_wsdev_event_date',
                    'value' => $current_date,
                    'compare' => '>=',
                    ),
                ),
            'meta_key' => '_wsdev_event_date',
            'orderby' => 'meta_value',
            'order' => 'ASC'
        );
        $init_events = new WP_Query($init_args);
        // put all the post IDs into an array
        $post_ids = wp_list_pluck( $init_events->posts, 'ID' );

        // make zip code API call
        //
        $connect = curl_init('https://www.zipcodeapi.com/rest/1wnOjJvpE6kmFylXJG7VBCEqZ2ewy8HAE5oYOTos0IPFhaD1RLWsnBTnD8TcgBMV/radius.json/'.$zip.'/'.$radius.'/mile');
        curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connect, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($connect, CURLOPT_TIMEOUT, 180);
        $response_json = '';
        if ( ($response_json = curl_exec($connect) ) === false) {

            echo 'Curl error: ' . curl_error($connect);
            
        }

        else {
            $response = json_decode($response_json);
            curl_close($connect);
        }

        // make an array of zip codes from $response_json
        $zipcodes = [];
        foreach($response->zip_codes as $zipcode) {
            array_push($zipcodes, $zipcode->zip_code);
        }

        // check if zipcodes in $post_ids match with any zip in $zipcodes
        $matched_events = [];
        foreach($post_ids as $post) {
            if ( in_array( get_post_meta($post, '_wsdev_event_zip', true ), $zipcodes ) ) {
                array_push($matched_events, $post);
            }
        }

        $no_matches = false;
        if (empty($matched_events)) {
        	$no_matches = true;
        }

        wp_reset_query();
        wp_reset_postdata();
        
    }

	switch($search_term) {
		
		case 'topic':
			if ($topic != '') {
				$topic = serialize(strval($topic));
			}

			$args = array(
			'post_type' => 'event',
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => '_wsdev_event_topic',
					'value' => $topic,
					'compare' => 'LIKE'
					),
				array(
					'key' => '_wsdev_event_date',
					'value' => $current_date,
					'compare' => '>='
					),
				),
			'meta_key' => '_wsdev_event_date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC'
			);
		break;

	case 'state':
		$args = array(
		'post_type' => 'event',
		'posts_per_page' => -1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_wsdev_event_state',
				'value' => $state,
				'compare' => 'LIKE'
				),
			array(
				'key' => '_wsdev_event_date',
				'value' => $current_date,
				'compare' => '>=',
				),
			),
		'meta_key' => '_wsdev_event_date',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
		break;

	// case 'month':
	// 	$args = array(
	// 	'post_type' => 'event',
	// 	'posts_per_page' => -1,
	// 	'meta_query' => array(
	// 		'relation' => 'AND',
	// 		// array(
	// 		// 	'key' => '_wsdev_event_month',
	// 		// 	'value' => $month,
	// 		// 	'compare' => 'LIKE'
	// 		// 	),
	// 		array(
	// 			'key' => '_wsdev_event_date',
	// 			'value' => $current_date,
	// 			'compare' => '>='
	// 			),
	// 		),
	// 	'meta_key' => '_wsdev_event_day_start',
	// 	'orderby' => 'meta_value',
	// 	'order' => 'ASC',
	// 	);
	// 	break;

    case 'zip':
        $args = array(
        'post_type' => 'event',
        'post__in' => $matched_events,
        'meta_key' => '_wsdev_event_date',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'posts_per_page' => -1
        );
        break;

    case 'month':
        $args = array(
        'post_type' => 'event',
        'post__in' => $matched_events,
        'meta_key' => '_wsdev_event_date',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'posts_per_page' => -1
        );
        break;

	default:
		$args = array(
		'post_type' => 'event',
		'posts_per_page' => -1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_wsdev_event_date',
				'value' => $current_date,
				'compare' => '>='
				),
			),
		'meta_key' => '_wsdev_event_date',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
		break;
	}

	$loop_month = null;

	if (isset($no_matches) && $no_matches == true) { 
		echo '<h3>Sorry, there are no classes listed that match your search query. Please try again</h3>';
	}

	else {
		
		$events = new WP_Query($args);

		while($events->have_posts()) : $events->the_post(); 

			$bold = false;
			$bold_event = get_post_meta(get_the_ID(), '_wsdev_event_bold', true);
			if ($bold_event == 'on') {
				$bold = true;
			}

			$current_date_string = gmdate("m/d/Y", $current_date);
			$current_date_string = explode('/', $current_date_string);
			$current_year = $current_date_string[2];

			$event_date_string = gmdate("m/d/Y", get_post_meta(get_the_ID(), '_wsdev_event_date', true));
			$event_date = explode('/', $event_date_string);
			$event_month = $event_date[0];
			$event_day = $event_date[1];
			$event_year = $event_date[2];

			$event_end_date_string = gmdate("m/d/Y", get_post_meta(get_the_ID(), '_wsdev_event_date_end', true));
			$event_end_date = explode('/', $event_end_date_string);
			$event_end_day = $event_end_date[1];

			$display_month = array(
					'01' => 'January',
					'02' => 'February',
					'03' => 'March',
					'04' => 'April',
					'05' => 'May',
					'06' => 'June',
					'07' => 'July',
					'08' => 'August',
					'09' => 'September',
					'10' => 'October',
					'11' => 'November',
					'12' => 'December');


			if ($loop_month != $event_month) {
				?>
	            </table>
	            <table class="table table-striped table-bordered table-condensed">
	                <tbody class="searchable">
				         <tr class="table-month-divider">
	        		            <td colspan="3">
	        			             <?php echo $display_month[$event_month]; ?>
	        			             <?php
	        				            if ($current_year < $event_year) {
	        					           echo '&nbsp;'.$event_year;
	        				            }
	        			             ?>
	        		            </td>
	        	           </tr>
	                    <?php
			          }

			
				 ?>
	        	<tr>
	        		<td class="date-column"><a href="<?php echo get_the_permalink(); ?>" <?php if($bold) { echo 'style="font-weight: bold"'; }?>><?php echo $event_day; ?>
	        			<?php if ( $event_day != '' && 
	        					$event_day != $event_end_day ) {
	        						echo '-'.$event_end_day;
	        					} ?></a>
	        		</td>
	        		<td class="title-column"><a href="<?php echo get_the_permalink(); ?>" <?php if($bold) { echo 'style="font-weight: bold"'; }?>><?php the_title(); ?></a></td>
	        		<td class="location-column"><a href="<?php echo get_the_permalink(); ?>" <?php if($bold) { echo 'style="font-weight: bold"'; }?>><?php echo get_post_meta(get_the_ID(), '_wsdev_event_city', true).', '.get_post_meta(get_the_ID(), '_wsdev_event_state', true); ?></a>
	        		</td>
	        	</tr>
	        	<?php
	        	$loop_month = $event_month; 
	         ?>
	<?php endwhile; 
} // end else
?>
