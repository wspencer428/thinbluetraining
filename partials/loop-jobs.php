<?php

    // variables from listing-jobs.php
    global $search_term;
    global $state;
    global $zip;
    global $radius;

	$current_date = date("m/d/Y");
	$current_date = strtotime($current_date);

    if ($search_term == 'zip') {
        $init_args = array(
            'post_type' => 'job',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_wsdev_job_close_date',
                    'value' => $current_date,
                    'compare' => '>=',
                    ),
                ),
            'meta_key' => '_wsdev_job_close_date',
            'orderby' => 'meta_value',
            'order' => 'ASC'
        );
        $init_jobs = new WP_Query($init_args);
        // put all the post IDs into an array
        $post_ids = wp_list_pluck( $init_jobs->posts, 'ID' );

        // make zip code API call
        //
        $response = zipRadius($zip, $radius);
        

        // make an array of zip codes from $response_json
        $zipcodes = [];
        foreach($response->zip_codes as $zipcode) {
            array_push($zipcodes, $zipcode->zip_code);
        }

        // check if zipcodes in $post_ids match with any zip in $zipcodes
        $matched_jobs = [];
        foreach($post_ids as $post) {
            if ( in_array( get_post_meta($post, '_wsdev_job_zip', true ), $zipcodes ) ) {
                array_push($matched_jobs, $post);
            }
        }

        $no_matches = false;
        if (empty($matched_jobs)) {
            $no_matches = true;
        }
        
    }

	switch($search_term) {

	case 'state':
		$args = array(
		'post_type' => 'job',
		'posts_per_page' => -1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_wsdev_job_state',
				'value' => $state,
				'compare' => 'LIKE'
				),
			array(
				'key' => '_wsdev_job_close_date',
				'value' => $current_date,
				'compare' => '>=',
				),
			),
		'meta_key' => '_wsdev_job_close_date',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
		break;

    case 'zip':
        $args = array(
        'post_type' => 'job',
        'post__in' => $matched_jobs,
        'meta_key' => '_wsdev_job_close_date',
        'orderby' => 'meta_value',
        'order' => 'ASC'
        );
        break;

	default:
		$args = array(
		'post_type' => 'job',
		'posts_per_page' => -1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_wsdev_job_close_date',
				'value' => $current_date,
				'compare' => '>='
				),
			),
		'meta_key' => '_wsdev_job_close_date',
		'orderby' => 'meta_value',
		'order' => 'ASC'
		);
		break;
	}

	$loop_month = null;

    if (isset($no_matches) && $no_matches == true) { 
        echo '<h3>Sorry, there are no classes listed that match your search query. Please try again</h3>';
    }
    
    else {
	
    	$jobs = new WP_Query($args);

    	while($jobs->have_posts()) : $jobs->the_post(); 

    ?>
    	

    	<?php

    		$current_date_string = gmdate("m/d/Y", $current_date);
    		$current_date_string = explode('/', $current_date_string);
    		$current_year = $current_date_string[2];

    		$job_date_string = gmdate("m/d/Y", get_post_meta(get_the_ID(), '_wsdev_job_close_date', true));
    		$job_date = explode('/', $job_date_string);
    		$job_month = $job_date[0];
    		$job_day = $job_date[1];
    		$job_year = $job_date[2];


    		$display_month = array(
    				'01' => 'January',
    				'02' => 'February',
    				'03' => 'March',
    				'04' => 'April',
    				'05' => 'May',
    				'06' => 'June',
    				'07' => 'July',
    				'08' => 'August',
    				'09' => 'September',
    				'10' => 'October',
    				'11' => 'November',
    				'12' => 'December');

    		

    		if ($loop_month != $job_month) {
    			?>
                </table>
                <table class="table table-striped table-bordered table-condensed">
                    <tbody class="searchable">
                         <tr class="table-month-divider">
                                <td colspan="3">
                        			<?php echo $display_month[$job_month]; ?>
                        			<?php
                        				if ($current_year < $job_year) {
                        					echo '&nbsp;'.$job_year;
                        				}
                        			?>
                        		</td>
                        	</tr>
                    <?php
    		  }

    		
    			?>
            	<tr>
            		<td class="date-column"><a href="<?php echo get_the_permalink(); ?>"><?php echo $job_day; ?></a></td>
            		<td class="title-column"><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></td>
            		<td class="location-column"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_post_meta(get_the_ID(), '_wsdev_job_city', true).', '.get_post_meta(get_the_ID(), '_wsdev_job_state', true); ?></a>
            		</td>
            	</tr>
            	<?php
            	$loop_month = $job_month; 
             ?>
    <?php endwhile; 
} // end else ?>