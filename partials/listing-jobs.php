<?php

	$search_term = '';
	if (isset($_GET['state'])) {
		$state = $_GET['state'];
		$search_term = 'state';
		if ($state == 'ALL') {
			$state = '';
		}
	}
	else {
		$state = '';
	}

	if (isset($_GET['zip'])) {
		$zip = $_GET['zip'];
		$search_term = 'zip';
	}

	else {
		$zip = '';
	}

	if (isset($_GET['radius'])) {
		$radius = $_GET['radius'];
		$search_term = 'zip';
	}

	else {
		$radius = '';
	}
	
?>

<a href="<?php bloginfo('url'); ?>/post-job" style="position: absolute;" class="recruiters-click-here"><img src="<?php bloginfo('stylesheet_directory'); ?>/dist/img/post-job.png" class="img-responsive" style="max-width: 260px; "/></a>

<form id="state_form" class="form-inline pull-right" style="text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/jobs/" method="GET">
	<div class="form-group">
		<label for="state">Search by State: </label>
		<select class="form-control" id="state_select" name="state" style="margin-top: 5px; width: 155px;">
			<option value="">Select a state</option>
			<option value="ALL">All States</option>
			<option value="AL">AL</option>
			<option value="AR">AR</option>
			<option value="AZ">AZ</option>
			<option value="CA">CA</option>
			<option value="CO">CO</option>
			<option value="CT">CT</option>
			<option value="DC">DC</option>
			<option value="DE">DE</option>
			<option value="FL">FL</option>
			<option value="GA">GA</option>
			<option value="HI">HI</option>
			<option value="IA">IA</option>
			<option value="ID">ID</option>
			<option value="IL">IL</option>
			<option value="IN">IN</option>
			<option value="KS">KS</option>
			<option value="KY">KY</option>
			<option value="LA">LA</option>
			<option value="MA">MA</option>
			<option value="MD">MD</option>
			<option value="ME">ME</option>
			<option value="MI">MI</option>
			<option value="MN">MN</option>
			<option value="MO">MO</option>
			<option value="MS">MS</option>
			<option value="MT">MT</option>
			<option value="NC">NC</option>
			<option value="ND">ND</option>
			<option value="NE">NE</option>
			<option value="NH">NH</option>
			<option value="NJ">NJ</option>
			<option value="NM">NM</option>
			<option value="NV">NV</option>
			<option value="NY">NY</option>
			<option value="OH">OH</option>
			<option value="OK">OK</option>
			<option value="OR">OR</option>
			<option value="PA">PA</option>
			<option value="RI">RI</option>
			<option value="SC">SC</option>
			<option value="SD">SD</option>
			<option value="TN">TN</option>
			<option value="TX">TX</option>
			<option value="UT">UT</option>
			<option value="VA">VA</option>
			<option value="VT">VT</option>
			<option value="WA">WA</option>
			<option value="WI">WI</option>
			<option value="WV">WV</option>
			<option value="WY">WY</option>
		</select>
	</div>
</form>
<br />
<form id="zip_form" class="form-inline pull-right" style="clear: both; text-align: right; margin-bottom: 15px;" action="<?php bloginfo('url'); ?>/jobs/" method="GET">
	<div class="form-group">
	<label for="zip">Search by zip code radius: </label>

		<input type="text" length="5" class="form-control" placeholder="5 digit" name="zip" style="margin-top: 5px; width: 90px;" />
		<select class="form-control" id="radius_select" name="radius" style="margin-top: 5px; width: 90px;">
			<option value="25">25 miles</option>
			<option value="50">50 miles</option>
			<option value="75">75 miles</option>
			<option value="100">100 miles</option>
			<option value="250">250 miles</option>
			<option value="500">500 miles</option>
		</select>
	</div>
	<br />
	<div class="form-group" style="clear: both;">
		<button class="btn btn-primary" id="searchButton" style="margin-top: 20px;">Search</button>
	</div>
</form> 

<br />
<br />
<br />
<div class="input-group" style="clear: both; margin-top: 20px;"> <span class="input-group-addon">Filter</span>

	<input id="filter" type="text" class="form-control" placeholder="Filter events by Name, Close Date, City or State">
</div>
<table id="listing-table-head" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th style="width: 20%;">Close Date</th>
			<th style="width: 50%;">Title</th>
			<th style="width: 30%;">Location</th>
		</tr>
	</thead>
	
    <?php include(locate_template('partials/loop-jobs.php')); ?>
   
</table>