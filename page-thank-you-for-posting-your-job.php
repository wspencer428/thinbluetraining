<?php
/**
 * page-thank-you-for-posting-your-job.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            <?php the_content(); ?>
	            <p>Thanks for posting your job! We've been alerted to your new job posting and once we review the listing, the job will be posted with the other jobs here on Thin Blue Training.</p>

				<p>In the meantime, check out some <a href="<?php bloginfo('url'); ?>/calendar">upcoming classes</a>.</p>
        	<?php endwhile; ?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>