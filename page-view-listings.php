<?php
/**
 * page-view-listings.php
 *
 * @package bootstrapped
 */

get_header(); 

if ( !is_user_logged_in() ) {
    header('Location: ' . get_permalink(223));
}

$current_user = wp_get_current_user();

$user_info = get_userdata($current_user->ID);

if ( in_array('trainer', $user_info->roles) || get_the_author_meta('user_type', $current_user->ID) == 'trainer' ) {
	$is_trainer = true;
}
else {
	header('Location: ' . get_permalink(223));
}

?>
	<div class="row">
		<div class="col-md-7">
          	
	        <h1 class="page-title">Your Class Listings</h1> 
	        <table class="table table-condensed table-striped">
	        	<thead>
	        		<tr>
	        			<th>Class Title</th>
	        			<th>Class Start Date</th>
	        			<th></th>
	        		</tr>
	        	</thead>
	        	<tbody>
	            
	            <?php
	            	$args = array(
						'post_type' => 'event',
						'author' => $current_user->ID,
						'posts_per_page' => -1 );

					$listings = new WP_Query($args);

					while ($listings->have_posts()) : $listings->the_post(); ?>

					<tr>
						<td><?php the_title(); ?></td>
						<td><?php echo gmdate("m/d/Y", get_post_meta(get_the_ID(), '_wsdev_event_date', true)); ?></td>
						<td><a class="btn btn-sm btn-danger" href="<?php bloginfo('url'); ?>/post-class?post=<?php echo get_the_ID(); ?>">Edit/Delete</a></td>
					</tr>

				<?php endwhile; ?>
				</tbody>
			</table>

			<div class="row" style="text-align: center; margin-top: 25px;">
				<div class="col-xs-12">
					<a style="color: #FFFFFF;" href="<?php bloginfo('url'); ?>/post-class" class="btn btn-primary btn-lg">Post another class</a>
				</div>
			</div>

        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>