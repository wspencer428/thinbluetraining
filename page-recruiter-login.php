<?php
/**
 * page-recruiter-login.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
                <p><strong>WANT TO INCREASE YOUR NUMBER OF APPLICANTS? LIST YOUR CLASS HERE!</strong>

                <p>It’s completely free, now and always. Our goal is simple: to give officers opportunities.</p>

                <p>In order to list your job opening, you only have to <a href="<?php bloginfo('url'); ?>/recruiter-registration">register</a>. Already registered? Sign In. </p>
	            <?php the_content(); ?>
        	<?php endwhile; ?>
        </div>
        <input type="hidden" id="blogURL" value="<?php bloginfo('url'); ?>" />
        <script>

        	var blogURL = jQuery('#blogURL').val();
        	jQuery(document).ready(function() {
        		jQuery('#login a[href$="registration/"]').attr('href', blogURL+'/recruiter-registration/');
        	});
        	

        </script>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>