<?php
	/**
	  * Coupon Code functionality
	  */

	header("Access-Control-Allow-Origin: *");

	require_once("../../../wp-load.php");

	if ( isset( $_GET['couponCode']) && $_GET['couponCode'] != '' ) {
		$couponCode = strtoupper($_GET['couponCode']);

		// check if coupon code exists
		$coupon = get_posts('name='.$couponCode.'&post_type=coupon');
		
		if (!empty($coupon)) {
			$coupon_id = $coupon[0]->ID;

			// check if coupon has expired

			$expiration = get_post_meta( $coupon_id, '_wsdev_coupon_end_date', true);


			$current_date = date("m/d/Y");
			$today = strtotime($current_date);

			if ($expiration == '') {
				// there is no set expiration, so set $noExpiration == true
				$noExpiration = true;
			}

			
			if ($expiration != '' && $today < $expiration || $noExpiration == true) {
				
				// check if coupon quantity is sufficient
				$quantity = get_post_meta( $coupon_id, '_wsdev_coupon_quantity', true);

				if ($quantity == '') {
					// there is no set quantity, so set $unlimited == true
					$unlimited = true;
				}

				if ($quantity > 0 || $unlimited) {
					
					// the coupon is valid. Return the value of the coupon

					$value = get_post_meta( $coupon_id, '_wsdev_coupon_value', true);
					echo $value;
				}

				else {
					echo 'The coupon code you entered is no longer valid.';
				}

			}

			else {
				echo 'The coupon code you entered has expired.';
			}


		}

		else {
			echo 'The coupon code you entered is not valid.';
		}

		

		

	}

	else {
		echo 'You forgot to enter your coupon code.';
	}

	

