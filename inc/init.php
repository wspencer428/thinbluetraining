<?php
/**
 * bootstrapped theme init setup
 * functions defined in theme-functions.php
 *
 * @package bootstrapped
 */

if ( ! function_exists( 'wsdev_bootstrapped_setup' ) ) :

function wsdev_bootstrapped_setup() {


	// Clean up the head
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Prevent File Modifications
	if ( ! defined( 'DISALLOW_FILE_EDIT' ) ) {
		define( 'DISALLOW_FILE_EDIT', true );
	}

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// Add Image Sizes
	// add_image_size( $name, $width = 0, $height = 0, $crop = false );

	// Remove Dashboard Meta Boxes
	// Function location: /inc/theme-functions.php
	add_action( 'wp_dashboard_setup', 'wsdev_remove_dashboard_widgets' );

	// Remove admin bar
	add_action('after_setup_theme', 'wsdev_remove_admin_bar');
	add_filter('show_admin_bar', '__return_false');


	// Change Admin Menu Order
	// Function location: /inc/theme-functions.php
	add_filter( 'custom_menu_order', '__return_true' );
	//add_filter( 'menu_order', 'wsdev_custom_menu_order' );

	// Hide Admin Areas that are not used
	// Function location: /inc/theme-functions.php
	add_action( 'admin_menu', 'wsdev_remove_menu_pages' );

	// Remove default link for images
	// Function location: /inc/theme-functions.php
	add_action( 'admin_init', 'wsdev_imagelink_setup', 10 );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Enqueue scripts
	// Function location: /inc/theme-functions.php
	add_action( 'wp_enqueue_scripts', 'wsdev_scripts' );

	// Remove Query Strings From Static Resources
	// Function location: /inc/theme-functions.php
	add_filter( 'script_loader_src', 'wsdev_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', 'wsdev_remove_script_version', 15, 1 );

	// Remove Read More Jump
	// Function location: /inc/theme-functions.php
	add_filter( 'the_content_more_link', 'wsdev_remove_more_jump_link' );

	// Extra author profile fields
	// Function location: /inc/theme-functions.php
	add_action( 'show_user_profile', 'wsdev_extra_user_profile_fields' );
	add_action( 'edit_user_profile', 'wsdev_extra_user_profile_fields' );
	add_action( 'personal_options_update', 'wsdev_save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'wsdev_save_extra_user_profile_fields' );
	add_action( 'user_register', 'wsdev_save_extra_user_profile_fields', 10, 1 );

	// Member functions
	// Function location: /inc/members.php
	add_action( 'init', 'wsdev_add_member_role' );
	add_filter( 'media_view_strings', 'wsdev_member_media_view_strings', 99 );
	add_filter( 'media_upload_tabs', 'wsdev_member_media_view_tabs', 99 );
	add_action( 'admin_init', 'wsdev_member_admin_prevent', 1 );
	add_filter( 'login_url', 'wsdev_login_page', 10, 2 );
	add_action( 'user_register', 'wsdev_add_to_aweber', 10, 1 );

	// Trainer functions
	// Function location: /inc/trainers.php
	add_action( 'init', 'wsdev_add_trainer_role' );
	add_filter( 'media_view_strings', 'wsdev_trainer_media_view_strings', 99 );
	add_filter( 'media_upload_tabs', 'wsdev_trainer_media_view_tabs', 99 );
	add_action( 'admin_init', 'wsdev_trainer_admin_prevent', 1 );

	// Add custom post types, required meta boxes and required custom functions
	//
	// EVENT CPT
	// Function location: /inc/cpt/event.php
	add_action( 'init', 'wsdev_event_cpt', 0 );
	add_action( 'cmb2_init', 'wsdev_register_event_metabox', 0 );

	// JOB CPT
	// Function location: /inc/cpt/job.php
	add_action( 'init', 'wsdev_job_cpt', 0 );
	add_action( 'cmb2_init', 'wsdev_register_job_metabox', 0 );

	// COUPON CPT
	// Function location: /inc/cpt/coupon.php
	add_action( 'init', 'wsdev_coupon_cpt', 0 );
	add_action( 'cmb2_init', 'wsdev_register_coupon_metabox', 0 );
	add_filter( 'manage_edit-coupon_columns', 'set_custom_coupon_columns' );
	add_action( 'manage_coupon_posts_custom_column' , 'custom_coupon_column', 10, 2 );

	// WP FrontEnd User Pro Customizations
	// add_action( 'wpuf_add_post_after_insert', 'convert_date_to_timestamp' );
	// add_action( 'wpuf_edit_post_after_update', 'convert_date_to_timestamp' );

	// SMS alerts for when new events are added
	//add_action( 'save_post', 'wsdev_send_sms_for_event', 10, 3 );

}
endif; // wsdev_bootstrapped_setup

add_action( 'after_setup_theme', 'wsdev_bootstrapped_setup' );
