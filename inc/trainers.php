<?php
/**
 * Trainer related functions
 * functions initialized with actions/filters in /lib/init.php
 *
 * @package bootstrapped
 */

/**
 * Create new "trainer" role
 */
function wsdev_add_trainer_role() {
    add_role( 'trainer', 'Trainer', array( 
										'read' => true, 
										'edit_post' => true,
										'delete_post' => true,
										'edit_published_post' => true,
										'delete_published_post' => true,
										'publish_post' => true,
										'upload_files' => false,
										'unfiltered_html' => true
									) );
}

/**
 * Remove items from Media Uploader if current user has a Trainer role
 */
function wsdev_trainer_media_view_strings( $strings ) {
	$author_data = get_userdata( get_current_user_id() );
	$author_role = implode(', ', $author_data->roles);
	if ( $author_role == 'trainer' ) {
		unset( $string['uploadImagesTitle'] );
		unset( $strings['uploadFilesTitle'] );
	    unset( $strings['createGalleryTitle'] );
	}
	
	return $strings;
}

/**
 * Remove items from Media Uploader if current user has a Trainer role
 */
function wsdev_trainer_media_view_tabs($tabs) {
	$author_data = get_userdata( get_current_user_id() );
	$author_role = implode(', ', $author_data->roles);
	if ( $author_role == 'trainer' ) {
		unset($tabs['gallery']);
	}

	return $tabs;
	
}

/**
 * Prevent Trainers from accessing the dashboard
 */
function wsdev_trainer_admin_prevent() {

	$current_user = wp_get_current_user();

	$user_info = get_userdata($current_user->ID);

	if (!$user_info) {
		return;
	}

	if ( in_array('trainer', $user_info->roles) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
		wp_redirect( site_url() ); 
		exit;
	}
}