<?php
/**
 * Member related functions
 * functions initialized with actions/filters in /lib/init.php
 *
 * @package bootstrapped
 */

/**
 * Create new "member" role
 */
function wsdev_add_member_role() {
    add_role( 'member', 'Member', array( 
										'read' => true, 
										'edit_post' => false,
										'delete_post' => false,
										'edit_published_post' => false,
										'delete_published_post' => false,
										'publish_post' => false,
										'upload_files' => false,
										'unfiltered_html' => true
									) );
}

/**
 * Remove items from Media Uploader if current user has a Member role
 */
function wsdev_member_media_view_strings( $strings ) {
	$author_data = get_userdata( get_current_user_id() );
	$author_role = implode(', ', $author_data->roles);
	if ( $author_role == 'member' ) {
		unset( $string['uploadImagesTitle'] );
		unset( $strings['uploadFilesTitle'] );
	    unset( $strings['createGalleryTitle'] );
	}
	
	return $strings;
}

/**
 * Remove items from Media Uploader if current user has a Member role
 */
function wsdev_member_media_view_tabs($tabs) {
	$author_data = get_userdata( get_current_user_id() );
	$author_role = implode(', ', $author_data->roles);
	if ( $author_role == 'member' ) {
		unset($tabs['gallery']);
	}

	return $tabs;
	
}

/**
 * Prevent "Members" from accessing the dashboard
 */
function wsdev_member_admin_prevent() {

	$current_user = wp_get_current_user();

	$user_info = get_userdata($current_user->ID);

	if (!$user_info) {
		return;
	}

	if ( in_array('member', $user_info->roles) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
		wp_redirect( site_url() ); 
		exit;
	}

	if ( in_array('trainer', $user_info->roles) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
		wp_redirect( site_url() ); 
		exit;
	}
}

/**
 * Add custom fields to user profile pages
 */
function wsdev_extra_user_profile_fields( $user ) { 
	?>
	<h3><?php _e("Extra profile information", "blank"); ?></h3>
	 
	<table class="form-table">
		<tr>
			<th><label for="user_phone"><?php _e("Phone Number"); ?></label></th>
			<td>
			<input type="text" name="user_phone" id="user_phone" value="<?php echo esc_attr( get_the_author_meta( 'user_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description"><?php _e("Please enter your phone number."); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="user_zip"><?php _e("Zip Code"); ?></label></th>
			<td>
			<input type="text" name="user_zip" id="user_zip" maxlength="5" value="<?php echo esc_attr( get_the_author_meta( 'user_zip', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description"><?php _e("Please enter your zip code."); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="sms_alert"><?php _e("Receive SMS Alerts"); ?></label></th>
			<td>
			<input type="checkbox" name="sms_alert" id="sms_alert" <?php if( get_the_author_meta( 'sms_alert', $user->ID )  == 'on' ) { ?>checked="checked"<?php } ?> class="widefat" /><br />
			<span class="description"><?php _e("Check if you wish to receive SMS alerts."); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="email_alert"><?php _e("Receive Email Alerts"); ?></label></th>
			<td>
			<input type="checkbox" name="email_alert" id="email_alert" <?php if( get_the_author_meta( 'email_alert', $user->ID )  == 'on' ) { ?>checked="checked"<?php } ?> class="widefat" /><br />
			<span class="description"><?php _e("Check if you wish to receive email alerts."); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="alert_radius"><?php _e("Alert Radius"); ?></label></th>
			<td>
			<select name="alert_radius" id="alert_radius" class="postbox">
				<option value="">--</option>
		        <option value="25" <?php if ( get_the_author_meta( 'alert_radius', $user->ID ) == 25 ) { echo "selected='selected'"; } ?>>25 miles</option>
		        <option value="50" <?php if ( get_the_author_meta( 'alert_radius', $user->ID ) == 50 ) { echo "selected='selected'"; } ?>>50 miles</option>
		        <option value="100" <?php if ( get_the_author_meta( 'alert_radius', $user->ID ) == 100 ) { echo "selected='selected'"; } ?>>100 miles</option>
		    </select>
			<span class="description"><?php _e("Selet the event radius you'd like to receive alerts for."); ?></span>
			</td>
		</tr>
		
		

	</table>
	<?php 
}
 
function wsdev_save_extra_user_profile_fields( $user_id ) {
 
	if ( !current_user_can( 'edit_user', $user_id ) ) { 
		return false; 
	}
 
 	$phoneNumeric = preg_replace('/\D/', '', $_POST['user_phone']);
	update_user_meta( $user_id, 'phone', $phoneNumeric );
	update_user_meta( $user_id, 'sms_alert', $_POST['sms_alert'] );
	update_user_meta( $user_id, 'email_alert', $_POST['email_alert'] );
	update_user_meta( $user_id, 'alert_radius', $_POST['alert_radius'] );
	$zipNumeric = preg_replace('/\D/', '', $_POST['user_zip']);
	update_user_meta( $user_id, 'user_zip', $zipNumeric );

	

}

function addToAweberMain($email_address, $first_name) {

	$list_id = '4015964'; // list name:Thin Blue Training 

	$app = new AweberConnect();

    $is_subscribed = $app->findSubscriber($email=$email_address);

    if (!$subscribed) {
		$list = $app->findList($name='Thin Blue Training');

	    $subscriber = array(
	        'email' => $email_address,
	        'name'  => $first_name,
	    );

	    $app->addSubscriber($subscriber, $list);
	}
	else {
		return;
	}

}

function addToAweberTrainer($email_address, $first_name, $organization) {

	$list_id = '4015972'; // list name:TBT Trainers 

	$app = new AweberConnect();

    $is_subscribed = $app->findSubscriber($email=$email_address);

    if (!$subscribed) {

	    $list = $app->findList($name='TBT Trainers');

	    $subscriber = array(
	        'email' => $email_address,
	        'name'  => $first_name,
	        'custom_fields' => array(
	            'Organization' => $organization,
	        ),
	    );

	    $app->addSubscriber($subscriber, $list);
	}

	else {
		return;
	}


}

function wsdev_add_to_aweber($user_id) {

	$user_info = get_userdata($user_id);

	
	if ( in_array('trainer', $user_info->roles) ){
		$is_trainer = true;
	}
	else {
		$is_trainer = false;
	}

	if($is_trainer) {
		if (isset($_POST['user_organization'])) {
			$organization = $_POST['user_organization'];
		}
		else {
			$organization = 'None provided';
		}
		addToAweberTrainer($_POST['email'], $_POST['first_name'], $organization);
	}
	
	else {
		addToAweberMain($_POST['email'], $_POST['first_name']);
	}
	
}

function wsdev_login_page( $login_url, $redirect ) {
    return home_url( '/login');
}

