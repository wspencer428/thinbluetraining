<?php
/**
 * Event custom post type functions
 * functions initialized with actions/filters in /lib/init.php
 *
 * @package bootstrapped
 */

/**
 * Event CPT
 */
function wsdev_event_cpt() {
    register_post_type( 'event',
        array(
            'labels' => array(
                'name' => __( 'Events' ),
                'singular_name' => __( 'Event' ),
                'add_new' => __( 'Add New Event' ),
                'add_new_item' => __( 'Add New Event' ),
                'edit_item' => __( 'Edit Event' ),
                'new_item' => __( 'Add New Event' ),
                'view_item' => __( 'View Event' ),
                'search_items' => __( 'Search Events' ),
                'not_found' => __( 'No events found' ),
                'not_found_in_trash' => __( 'No events found in trash' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'custom-fields', ),
            'taxonomies' => array('category'),
            'capability_type' => 'post',
            'rewrite'         => array(
                    'slug'          => 'calendar',
                    'with_front'    => true
                ),
            'menu_position' => 9
        )
    );
};

/**
 * Event meta boxes for back-end posting and setup of data fields for front-end posting
 */

function wsdev_register_event_metabox() {
    
    $prefix = '_wsdev_';
    /**
     * Start the main metabox
     */
    $event_cmb = new_cmb2_box( array(
        'id'            => $prefix . 'event_metabox',
        'title'         => __( 'Event Details', 'cmb2' ),
        'object_types'  => array( 'event', ), // Post type
        // 'show_on_cb' => 'wsdev_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );
    /**
     * Individual meta fields
     */
    $event_cmb->add_field( array(
        'name' => __( 'Event start date', 'cmb2' ),
        'desc' => __( 'Starting date of event', 'cmb2' ),
        'id'   => $prefix . 'event_date',
        'type' => 'text_date_timestamp',
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'Event end date', 'cmb2' ),
        'desc' => __( 'Ending date of event', 'cmb2' ),
        'id'   => $prefix . 'event_date_end',
        'type' => 'text_date_timestamp',
    ) );

    $event_cmb->add_field( array(
        'name'       => __( 'Street Address', 'cmb2' ),
        'desc'       => __( 'Street address of event', 'cmb2' ),
        'id'         => $prefix . 'event_address',
        'type'       => 'text',
        'show_on_cb' => 'wsdev_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'City', 'cmb2' ),
        'desc' => __( 'City of event', 'cmb2' ),
        'id'   => $prefix . 'event_city',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $event_cmb->add_field( array(
        'name'             => __( 'State', 'cmb2' ),
        'desc'             => __( 'State of event', 'cmb2' ),
        'id'               => $prefix . 'event_state',
        'type'             => 'select',
        'show_option_none' => true,
        'options'          => array(
            'AL' => __( 'AL', 'cmb2' ),
            'AK' => __( 'AK', 'cmb2' ),
            'AR' => __( 'AR', 'cmb2' ),
            'AZ' => __( 'AZ', 'cmb2' ),
            'CA' => __( 'CA', 'cmb2' ),
            'CO' => __( 'CO', 'cmb2' ),
            'CT' => __( 'CT', 'cmb2' ),
            'DC' => __( 'DC', 'cmb2' ),
            'DE' => __( 'DE', 'cmb2' ),
            'FL' => __( 'FL', 'cmb2' ),
            'GA' => __( 'GA', 'cmb2' ),
            'HI' => __( 'HI', 'cmb2' ),
            'IA' => __( 'IA', 'cmb2' ),
            'ID' => __( 'ID', 'cmb2' ),
            'IL' => __( 'IL', 'cmb2' ),
            'IN' => __( 'IN', 'cmb2' ),
            'KS' => __( 'KS', 'cmb2' ),
            'KY' => __( 'KY', 'cmb2' ),
            'LA' => __( 'LA', 'cmb2' ),
            'MA' => __( 'MA', 'cmb2' ),
            'MD' => __( 'MD', 'cmb2' ),
            'ME' => __( 'ME', 'cmb2' ),
            'MI' => __( 'MI', 'cmb2' ),
            'MN' => __( 'MN', 'cmb2' ),
            'MO' => __( 'MO', 'cmb2' ),
            'MS' => __( 'MS', 'cmb2' ),
            'MT' => __( 'MT', 'cmb2' ),
            'NC' => __( 'NC', 'cmb2' ),
            'ND' => __( 'ND', 'cmb2' ),
            'NE' => __( 'NE', 'cmb2' ),
            'NH' => __( 'NH', 'cmb2' ),
            'NJ' => __( 'NJ', 'cmb2' ),
            'NM' => __( 'NM', 'cmb2' ),
            'NV' => __( 'NV', 'cmb2' ),
            'NY' => __( 'NY', 'cmb2' ),
            'OH' => __( 'OH', 'cmb2' ),
            'OK' => __( 'OK', 'cmb2' ),
            'OR' => __( 'OR', 'cmb2' ),
            'PA' => __( 'PA', 'cmb2' ),
            'RI' => __( 'RI', 'cmb2' ),
            'SC' => __( 'SC', 'cmb2' ),
            'SD' => __( 'SD', 'cmb2' ),
            'TN' => __( 'TN', 'cmb2' ),
            'TX' => __( 'TX', 'cmb2' ),
            'UT' => __( 'UT', 'cmb2' ),
            'VA' => __( 'VA', 'cmb2' ),
            'VT' => __( 'VT', 'cmb2' ),
            'WA' => __( 'WA', 'cmb2' ),
            'WI' => __( 'WI', 'cmb2' ),
            'WV' => __( 'WV', 'cmb2' ),
            'WY' => __( 'WY', 'cmb2' ),
        ),
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'Zip code', 'cmb2' ),
        'desc' => __( 'Zip code of event', 'cmb2' ),
        'id'   => $prefix . 'event_zip',
        'type' => 'text_small',
        // 'repeatable' => true,
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'Cost', 'cmb2' ),
        'desc' => __( 'Total cost for one student (additional cost information can be included in description)', 'cmb2' ),
        'id'   => $prefix . 'event_cost',
        'type' => 'text_money',
        // 'before_field' => '£', // override '$' symbol if needed
        // 'repeatable' => true,
    ) );
    
    $event_cmb->add_field( array(
        'name' => __( 'Website URL', 'cmb2' ),
        'desc' => __( 'Official website for event', 'cmb2' ),
        'id'   => $prefix . 'event_website',
        'type' => 'text_url',
        // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
        // 'repeatable' => true,
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'Contact person', 'cmb2' ),
        'desc' => __( 'Official contact person for event, if any', 'cmb2' ),
        'id'   => $prefix . 'event_contact_person',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );


    $event_cmb->add_field( array(
        'name' => __( 'Contact Email', 'cmb2' ),
        'desc' => __( 'Contact e-mail address', 'cmb2' ),
        'id'   => $prefix . 'event_contact_email',
        'type' => 'text_email',
        // 'repeatable' => true,
    ) );

    $event_cmb->add_field( array(
        'name' => __( 'Contact phone', 'cmb2' ),
        'desc' => __( 'Official contact person for event, if any', 'cmb2' ),
        'id'   => $prefix . 'event_contact_phone',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    
    $event_cmb->add_field( array(
        'name'    => __( 'Event topics', 'cmb2' ),
        'desc'    => __( 'Select topics based on the event training', 'cmb2' ),
        'id'      => $prefix . 'event_topic',
        'type'    => 'multicheck',
        // 'multiple' => true, // Store values in individual rows
        'options' => array(
            'Accident Investigation' => __( 'Accident Investigation', 'cmb2' ),
            'Accident Reconstruction' => __( 'Accident Reconstruction', 'cmb2' ),
            'Active Shooter' => __( 'Active Shooter', 'cmb2' ),
            'Administration' => __( 'Administration', 'cmb2' ),
            'Aquatic Investigations' => __( 'Aquatic Investigations', 'cmb2' ),
            'Ambush' => __( 'Ambush', 'cmb2' ),
            'Armorer' => __( 'Armorer', 'cmb2' ),
            'Arrest & Control' => __( 'Arrest & Control', 'cmb2' ),
            'Arson' => __( 'Arson', 'cmb2' ),
            'Asset Forfeiture' => __( 'Asset Forfeiture', 'cmb2' ),
            'Assault' => __( 'Assault', 'cmb2' ),
            'Autism' => __( 'Autism', 'cmb2' ),
            'Auto Theft' => __( 'Auto Theft', 'cmb2' ),
            'Background Investigation' => __( 'Background Investigation', 'cmb2' ),
            'Bicycle Accidents' => __( 'Bicycle Accidents', 'cmb2' ),
            'Bicycle Operations' => __( 'Bicycle Operations', 'cmb2' ),
            'Body Worn Cameras' => __( 'Body Worn Cameras', 'cmb2' ),
            'Breaching' => __( 'Breaching', 'cmb2' ),
            'Budgeting' => __( 'Budgeting', 'cmb2' ),
            'Burglary' => __( 'Burglary', 'cmb2' ),
            'Cameras- Body Worn' => __( 'Cameras- Body Worn', 'cmb2' ),
            'Canine' => __( 'Canine', 'cmb2' ),
            'Cartel' => __( 'Cartel', 'cmb2' ),
            'Case Management' => __( 'Case Management', 'cmb2' ),
            'Casualty Training' => __( 'Casualty Training', 'cmb2' ),
            'Cellphone Investigations' => __( 'Cellphone Investigations', 'cmb2' ),
            'Chemical Aerosol' => __( 'Chemical Aerosol', 'cmb2' ),
            'Child Abuse' => __( 'Child Abuse', 'cmb2' ),
            'Child Death' => __( 'Child Death', 'cmb2' ),
            'Child Exploitation' => __( 'Child Exploitation', 'cmb2' ),
            'Child Porn' => __( 'Child Porn', 'cmb2' ),
            'Citizen Academies' => __( 'Citizen Academies', 'cmb2' ),
            'College & University' => __( 'College & University', 'cmb2' ),
            'Communications' => __( 'Communications', 'cmb2' ),
            'Community Policing' => __( 'Community Policing', 'cmb2' ),
            'Computer Crimes' => __( 'Computer Crimes', 'cmb2' ),
            'Computer Forensics' => __( 'Computer Forensics', 'cmb2' ),
            'Concealment' => __( 'Concealment', 'cmb2' ),
            'Conference & Events' => __( 'Conference & Events', 'cmb2' ),
            'Confidential Informants' => __( 'Confidential Informants', 'cmb2' ),
            'Conflict Resolution' => __( 'Conflict Resolution', 'cmb2' ),
            'Confrontation Avoidance' => __( 'Confrontation Avoidance', 'cmb2' ),
            'Conservation Law Enforcement' => __( 'Conservation Law Enforcement', 'cmb2' ),
            'Corrections' => __( 'Corrections', 'cmb2' ),
            'Courthouse' => __( 'Courthouse ', 'cmb2' ),
            'Courtroom Testimony' => __( 'Courtroom Testimony', 'cmb2' ),
            'Craigslist' => __( 'Craigslist', 'cmb2' ),
            'Critical Incident Command' => __( 'Critical Incident Command', 'cmb2' ),
            'Crime Prevention' => __( 'Crime Prevention', 'cmb2' ),
            'Crime Scene Investigation' => __( 'Crime Scene Investigation', 'cmb2' ),
            'Crisis & Hostage' => __( 'Crisis & Hostage', 'cmb2' ),
            'Custodial Death' => __( 'Custodial Death', 'cmb2' ),
            'Cyberbullying' => __( 'Cyberbullying', 'cmb2' ),
            'Deaf & Hard of Hearing' => __( 'Deaf & Hard of Hearing', 'cmb2' ),
            'Deescalation' => __( 'Deescalation ', 'cmb2' ),
            'Defensive Tactics' => __( 'Defensive Tactics', 'cmb2' ),
            'Detective Workshop' => __( 'Detective Workshop', 'cmb2' ),
            'Dispatcher' => __( 'Dispatcher', 'cmb2' ),
            'Disruptive Students' => __( 'Disruptive Students', 'cmb2' ),
            'Domestic Violence' => __( 'Domestic Violence', 'cmb2' ),
            'Drug Diversion' => __( 'Drug Diversion', 'cmb2' ),
            'Drugs' => __( 'Drugs', 'cmb2' ),
            'DUI' => __( 'DUI', 'cmb2' ),
            'DWI' => __( 'DWI', 'cmb2' ),
            'Electronic Control Device' => __( 'Electronic Control Device', 'cmb2' ),
            'Electronic Countermeasures' => __( 'Electronic Countermeasures', 'cmb2' ),
            'Emergency Medicine' => __( 'Emergency Medicine', 'cmb2' ),
            'EMT' => __( 'EMT', 'cmb2' ),
            'Entry' => __( 'Entry', 'cmb2' ),
            'Evaluation' => __( 'Evaluation', 'cmb2' ),
            'Evidence' => __( 'Evidence', 'cmb2' ),
            'Executive Protection' => __( 'Executive Protection', 'cmb2' ),
            'Expandable Baton' => __( 'Expandable Baton', 'cmb2' ),
            'Explosives' => __( 'Explosives', 'cmb2' ),
            'Explosive Breaching' => __( 'Explosive Breaching', 'cmb2' ),
            'Facebook' => __( 'Facebook', 'cmb2' ),
            'Fatality Accident Investigation' => __( 'Fatality Accident Investigation', 'cmb2' ),
            'Field Training Officer' => __( 'Field Training Officer', 'cmb2' ),
            'Financial' => __( 'Financial', 'cmb2' ),
            'Fingerprinting' => __( 'Fingerprinting', 'cmb2' ),
            'Firearms' => __( 'Firearms', 'cmb2' ),
            'Fitness' => __( 'Fitness', 'cmb2' ),
            'Foreign Languages' => __( 'Foreign Languages', 'cmb2' ),
            'Forensics' => __( 'Forensics', 'cmb2' ),
            'Fraud Investigations' => __( 'Fraud Investigations', 'cmb2' ),
            'Gaming' => __( 'Gaming', 'cmb2' ),
            'Gangs' => __( 'Gangs', 'cmb2' ),
            'Google' => __( 'Google', 'cmb2' ),
            'GPS' => __( 'GPS', 'cmb2' ),
            'Grant Writing' => __( 'Grant Writing', 'cmb2' ),
            'Ground Control' => __( 'Ground Control', 'cmb2' ),
            'Handcuffing' => __( 'Handcuffing', 'cmb2' ),
            'Handgun' => __( 'Handgun', 'cmb2' ),
            'HazMat' => __( 'HazMat', 'cmb2' ),
            'High Crime Areas' => __( 'High Crime Areas', 'cmb2' ),
            'High Voltage Training' => __( 'High Voltage Training', 'cmb2' ),
            'Homicide' => __( 'Homicide ', 'cmb2' ),
            'Hostage Negotiation' => __( 'Hostage Negotiation', 'cmb2' ),
            'Immigration' => __( 'Immigration', 'cmb2' ),
            'Infanticide' => __( 'Infanticide', 'cmb2' ),
            'Injury Prevention' => __( 'Injury Prevention', 'cmb2' ),
            'In-Service Training' => __( 'In-Service Training', 'cmb2' ),
            'Instructor' => __( 'Instructor', 'cmb2' ),
            'Intelligence' => __( 'Intelligence', 'cmb2' ),
            'Interdiction' => __( 'Interdiction', 'cmb2' ),
            'Internal Affairs' => __( 'Internal Affairs', 'cmb2' ),
            'Internet Investigations' => __( 'Internet Investigations', 'cmb2' ),
            'Internet Protocol Cameras' => __( 'Internet Protocol Cameras', 'cmb2' ),
            'Interviews' => __( 'Interviews', 'cmb2' ),
            'Investigations' => __( 'Investigations', 'cmb2' ),
            'Jail Operations' => __( 'Jail Operations', 'cmb2' ),
            'Juvenile Investigations' => __( 'Juvenile Investigations', 'cmb2' ),
            'K-9' => __( 'K-9', 'cmb2' ),
            'Knife Defense' => __( 'Knife Defense', 'cmb2' ),
            'Langauges' => __( 'Languages', 'cmb2' ),
            'Leadership' => __( 'Leadership', 'cmb2' ),
            'Legal' => __( 'Legal', 'cmb2' ),
            'Less Lethal' => __( 'Less Lethal', 'cmb2' ),
            'Liability Management' => __( 'Liability Management', 'cmb2' ),
            'Light Energy Applications' => __( 'Light Energy Applications', 'cmb2' ),
            'Lock Picking' => __( 'Lock Picking', 'cmb2' ),
            'Management' => __( 'Management', 'cmb2' ),
            'Marine Operations' => __( 'Marine Operations', 'cmb2' ),
            'Media Relations' => __( 'Media Relations', 'cmb2' ),
            'Mental Illness' => __( 'Mental Illness', 'cmb2' ),
            'Mentoring' => __( 'Mentoring', 'cmb2' ),
            'Minority Relations' => __( 'Minority Relations', 'cmb2' ),
            'Miscellaneous' => __( 'Miscellaneous', 'cmb2' ),
            'Motorcycle Gangs' => __( 'Motorcycle Gangs', 'cmb2' ),
            'Motorcycle Operations' => __( 'Motorcycle Operations', 'cmb2' ),
            'Narcotics' => __( 'Narcotics', 'cmb2' ),
            'Neighborhood Watch' => __( 'Neighborhood Watch', 'cmb2' ),
            'Officer Involved Shooting' => __( 'Officer Involved Shooting', 'cmb2' ),
            'Officer Survival' => __( 'Officer Survival', 'cmb2' ),
            'Open Source Investigations' => __( 'Open Source Investigations', 'cmb2' ),
            'Organized Crime' => __( 'Organized Crime', 'cmb2' ),
            'Pathology' => __( 'Pathology', 'cmb2' ),
            'Patrol' => __( 'Patrol', 'cmb2' ),
            'Patrol Rifle' => __( 'Patrol Rifle', 'cmb2' ),
            'Pawn Shops' => __( 'Pawn Shops', 'cmb2' ),
            'Peer Support' => __( 'Peer Support', 'cmb2' ),
            'Pharmaceutical Investigations' => __( 'Pharmaceutical Investigations', 'cmb2' ),
            'Photography' => __( 'Photography', 'cmb2' ),
            'Pinterest' => __( 'Pinterest', 'cmb2' ),
            'Pistol' => __( 'Pistol', 'cmb2' ),
            'Policy & Procedure' => __( 'Policy & Procedure', 'cmb2' ),
            'Polygraph' => __( 'Polygraph', 'cmb2' ),
            'Powerpoint' => __( 'Powerpoint', 'cmb2' ),
            'Precursors' => __( 'Precursors', 'cmb2' ),
            'Promotional Testing' => __( 'Promotional Testing', 'cmb2' ),
            'Property Room' => __( 'Property Room', 'cmb2' ),
            'Public Information Officer' => __( 'Public Information Officer', 'cmb2' ),
            'Public Speaking' => __( 'Public Speaking', 'cmb2' ),
            'Recruitment' => __( 'Recruitment', 'cmb2' ),
            'Redman' => __( 'Redman', 'cmb2' ),
            'Report Writing' => __( 'Report Writing', 'cmb2' ),
            'Rifle' => __( 'Rifle', 'cmb2' ),
            'Riot Control' => __( 'Riot Control', 'cmb2' ),
            'Robbery' => __( 'Robbery', 'cmb2' ),
            'School Resource' => __( 'School Resource', 'cmb2' ),
            'SCUBA' => __( 'SCUBA', 'cmb2' ),
            'Search & Seizure' => __( 'Search & Seizure', 'cmb2' ),
            'Sex Crimes' => __( 'Sex Crimes', 'cmb2' ),
            'Sex Trafficking' => __( 'Sex Trafficking ', 'cmb2' ),
            'Shoothouse' => __( 'Shoothouse', 'cmb2' ),
            'Shotgun' => __( 'Shotgun', 'cmb2' ),
            'Sign Language' => __( 'Sign Language', 'cmb2' ),
            'Sniper' => __( 'Sniper', 'cmb2' ),
            'Social Media' => __( 'Social Media', 'cmb2' ),
            'Sovereign Citizens' => __( 'Sovereign Citizens', 'cmb2' ),
            'Specialized Entry Techniques' => __( 'Specialized Entry Techniques', 'cmb2' ),
            'Special Operations' => __( 'Special Operations', 'cmb2' ),
            'Street Crimes' => __( 'Street Crimes', 'cmb2' ),
            'Street Survival' => __( 'Street Survival', 'cmb2' ),
            'Subversive Groups' => __( 'Subversive Groups', 'cmb2' ),
            'Supervision' => __( 'Supervision', 'cmb2' ),
            'Suicide' => __( 'Suicide', 'cmb2' ),
            'Suicide Prevention' => __( 'Suicide Prevention', 'cmb2' ),
            'Surveillance' => __( 'Surveillance', 'cmb2' ),
            'SWAT' => __( 'SWAT', 'cmb2' ),
            'Tactical Medicine' => __( 'Tactical Medicine', 'cmb2' ),
            'Tactics' => __( 'Tactics', 'cmb2' ),
            'Technical Operations' => __( 'Technical Operations', 'cmb2' ),
            'Terrorism' => __( 'Terrorism', 'cmb2' ),
            'Threat Assessment' => __( 'Threat Assessment', 'cmb2' ),
            'Tracking' => __( 'Tracking', 'cmb2' ),
            'Traffic' => __( 'Traffic', 'cmb2' ),
            'Train the Trainer' => __( 'Train the Trainer', 'cmb2' ),
            'Twitter' => __( 'Twitter', 'cmb2' ),
            'Undercover' => __( 'Undercover', 'cmb2' ),
            'Use of Force' => __( 'Use of Force', 'cmb2' ),
            'Vehicular Operations' => __( 'Vehicular Operations', 'cmb2' ),
            'Verbal Conflict' => __( 'Verbal Conflict', 'cmb2' ),
            'Vice' => __( 'Vice', 'cmb2' ),
            'Victimology' => __( 'Victimology', 'cmb2' ),
            'Violent Crimes' => __( 'Violent Crimes', 'cmb2' ),
            'VIP Protection' => __( 'VIP Protection', 'cmb2' ),
            'Warrant Service' => __( 'Warrant Service', 'cmb2' ),
            'Water Survival' => __( 'Water Survival', 'cmb2' ),
            'Women in Law Enforcement' => __( 'Women in Law Enforcement', 'cmb2' ),

        ),
        // 'inline'  => true, // Toggles display to inline
    ) );

    $event_cmb->add_field( array(
        'name' => 'Bold Event',
        'desc' => 'Bold this event in class listings',
        'id'   => $prefix . 'event_bold',
        'type' => 'checkbox'
    ) );
    
}

