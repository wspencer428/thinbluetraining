<?php
/**
 * Job custom post type functions
 * functions initialized with actions/filters in /lib/init.php
 *
 * @package bootstrapped
 */

/**
 * Job CPT
 */
function wsdev_job_cpt() {
    register_post_type( 'job',
        array(
            'labels' => array(
                'name' => __( 'Jobs' ),
                'singular_name' => __( 'Job' ),
                'add_new' => __( 'Add New Job' ),
                'add_new_item' => __( 'Add New Job' ),
                'edit_item' => __( 'Edit Job' ),
                'new_item' => __( 'Add New Job' ),
                'view_item' => __( 'View Job' ),
                'search_items' => __( 'Search Jobs' ),
                'not_found' => __( 'No jobs found' ),
                'not_found_in_trash' => __( 'No jobs found in trash' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'author', 'custom-fields', ),
            'taxonomies' => array('category'),
            'capability_type' => 'post',
            'rewrite'         => array(
                    'slug'          => 'jobs',
                    'with_front'    => true
                ),
            'menu_position' => 9
        )
    );
};

/**
 * Event meta boxes for back-end posting and setup of data fields for front-end posting
 */

function wsdev_register_job_metabox() {
    
    $prefix = '_wsdev_';
    /**
     * Start the main metabox
     */
    $job_cmb = new_cmb2_box( array(
        'id'            => $prefix . 'job_metabox',
        'title'         => __( 'Job Details', 'cmb2' ),
        'object_types'  => array( 'job', ), // Post type
        // 'show_on_cb' => 'wsdev_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );
    /**
     * Individual meta fields
     */
    $job_cmb->add_field( array(
        'name' => __( 'Job Salary/Benefits', 'cmb2' ),
        'desc' => __( 'List salary and benefits included with job', 'cmb2' ),
        'id'   => $prefix . 'job_benefits',
        'type' => 'textarea',
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'Job open date', 'cmb2' ),
        'desc' => __( 'Opening date of job', 'cmb2' ),
        'id'   => $prefix . 'job_open_date',
        'type' => 'text_date_timestamp',
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'Job close date', 'cmb2' ),
        'desc' => __( 'Closing date of job', 'cmb2' ),
        'id'   => $prefix . 'job_close_date',
        'type' => 'text_date_timestamp',
    ) );

    $job_cmb->add_field( array(
        'name'       => __( 'Agency', 'cmb2' ),
        'desc'       => __( 'Hiring agency', 'cmb2' ),
        'id'         => $prefix . 'job_agency',
        'type'       => 'text',
        //'show_on_cb' => 'wsdev_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'City', 'cmb2' ),
        'desc' => __( 'Agency city', 'cmb2' ),
        'id'   => $prefix . 'job_city',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );

    $job_cmb->add_field( array(
        'name'             => __( 'State', 'cmb2' ),
        'desc'             => __( 'State of agency', 'cmb2' ),
        'id'               => $prefix . 'job_state',
        'type'             => 'select',
        'show_option_none' => true,
        'options'          => array(
            'AL' => __( 'AL', 'cmb2' ),
            'AK' => __( 'AK', 'cmb2' ),
            'AR' => __( 'AR', 'cmb2' ),
            'AZ' => __( 'AZ', 'cmb2' ),
            'CA' => __( 'CA', 'cmb2' ),
            'CO' => __( 'CO', 'cmb2' ),
            'CT' => __( 'CT', 'cmb2' ),
            'DC' => __( 'DC', 'cmb2' ),
            'DE' => __( 'DE', 'cmb2' ),
            'FL' => __( 'FL', 'cmb2' ),
            'GA' => __( 'GA', 'cmb2' ),
            'HI' => __( 'HI', 'cmb2' ),
            'IA' => __( 'IA', 'cmb2' ),
            'ID' => __( 'ID', 'cmb2' ),
            'IL' => __( 'IL', 'cmb2' ),
            'IN' => __( 'IN', 'cmb2' ),
            'KS' => __( 'KS', 'cmb2' ),
            'KY' => __( 'KY', 'cmb2' ),
            'LA' => __( 'LA', 'cmb2' ),
            'MA' => __( 'MA', 'cmb2' ),
            'MD' => __( 'MD', 'cmb2' ),
            'ME' => __( 'ME', 'cmb2' ),
            'MI' => __( 'MI', 'cmb2' ),
            'MN' => __( 'MN', 'cmb2' ),
            'MO' => __( 'MO', 'cmb2' ),
            'MS' => __( 'MS', 'cmb2' ),
            'MT' => __( 'MT', 'cmb2' ),
            'NC' => __( 'NC', 'cmb2' ),
            'ND' => __( 'ND', 'cmb2' ),
            'NE' => __( 'NE', 'cmb2' ),
            'NH' => __( 'NH', 'cmb2' ),
            'NJ' => __( 'NJ', 'cmb2' ),
            'NM' => __( 'NM', 'cmb2' ),
            'NV' => __( 'NV', 'cmb2' ),
            'NY' => __( 'NY', 'cmb2' ),
            'OH' => __( 'OH', 'cmb2' ),
            'OK' => __( 'OK', 'cmb2' ),
            'OR' => __( 'OR', 'cmb2' ),
            'PA' => __( 'PA', 'cmb2' ),
            'RI' => __( 'RI', 'cmb2' ),
            'SC' => __( 'SC', 'cmb2' ),
            'SD' => __( 'SD', 'cmb2' ),
            'TN' => __( 'TN', 'cmb2' ),
            'TX' => __( 'TX', 'cmb2' ),
            'UT' => __( 'UT', 'cmb2' ),
            'VA' => __( 'VA', 'cmb2' ),
            'VT' => __( 'VT', 'cmb2' ),
            'WA' => __( 'WA', 'cmb2' ),
            'WI' => __( 'WI', 'cmb2' ),
            'WV' => __( 'WV', 'cmb2' ),
            'WY' => __( 'WY', 'cmb2' ),
        ),
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'Zip code', 'cmb2' ),
        'desc' => __( 'Zip code of agency', 'cmb2' ),
        'id'   => $prefix . 'job_zip',
        'type' => 'text_small',
        // 'repeatable' => true,
    ) );
    
    $job_cmb->add_field( array(
        'name' => __( 'Website URL', 'cmb2' ),
        'desc' => __( 'Official website for job posting', 'cmb2' ),
        'id'   => $prefix . 'job_website',
        'type' => 'text_url',
        // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
        // 'repeatable' => true,
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'Contact person', 'cmb2' ),
        'desc' => __( 'Official contact person for job, if any', 'cmb2' ),
        'id'   => $prefix . 'job_contact_person',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );


    $job_cmb->add_field( array(
        'name' => __( 'Contact Email', 'cmb2' ),
        'desc' => __( 'Contact e-mail address', 'cmb2' ),
        'id'   => $prefix . 'job_contact_email',
        'type' => 'text_email',
        // 'repeatable' => true,
    ) );

    $job_cmb->add_field( array(
        'name' => __( 'Contact phone', 'cmb2' ),
        'desc' => __( 'Official contact person for job, if any', 'cmb2' ),
        'id'   => $prefix . 'job_contact_phone',
        'type' => 'text_medium',
        // 'repeatable' => true,
    ) );
    
}

