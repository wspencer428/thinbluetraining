<?php
/**
 * Coupon custom post type functions
 * functions initialized with actions/filters in /lib/init.php
 *
 * @package bootstrapped
 */

/**
 * Coupon CPT
 */
function wsdev_coupon_cpt() {
    register_post_type( 'coupon',
        array(
            'labels' => array(
                'name' => __( 'Coupon Codes' ),
                'singular_name' => __( 'Coupon Code' ),
                'add_new' => __( 'Add New Coupon Code' ),
                'add_new_item' => __( 'Add New Coupon Code' ),
                'edit_item' => __( 'Edit Coupon Code' ),
                'new_item' => __( 'Add New Coupon Code' ),
                'view_item' => __( 'View Coupon Code' ),
                'search_items' => __( 'Search Coupon Codes' ),
                'not_found' => __( 'No coupon codes found' ),
                'not_found_in_trash' => __( 'No coupon codes found in trash' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'title', ),
            'capability_type' => 'post',
            'menu_position' => 9
        )
    );
};

/**
 * Event meta boxes for back-end posting and setup of data fields for front-end posting
 */

function wsdev_register_coupon_metabox() {
    
    $prefix = '_wsdev_';
    /**
     * Start the main metabox
     */
    $coupon_cmb = new_cmb2_box( array(
        'id'            => $prefix . 'coupon_metabox',
        'title'         => __( 'Coupon Details', 'cmb2' ),
        'object_types'  => array( 'coupon', ), // Post type
        // 'show_on_cb' => 'wsdev_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
    ) );
    /**
     * Individual meta fields
     */
    $coupon_cmb->add_field( array(
        'name' => __( 'Value', 'cmb2' ),
        'desc' => __( 'Enter the percentage this coupon will deduct from the total purchase. Enter numbers ONLY.', 'cmb2' ),
        'id'   => $prefix . 'coupon_value',
        'type' => 'text',
    ) );

    $coupon_cmb->add_field( array(
        'name' => __( 'Coupon end date', 'cmb2' ),
        'desc' => __( 'Date coupon will expire', 'cmb2' ),
        'id'   => $prefix . 'coupon_end_date',
        'type' => 'text_date_timestamp',
    ) );

    $coupon_cmb->add_field( array(
        'name' => __( 'Quantity', 'cmb2' ),
        'desc' => __( 'Enter a quantity to limit the number of times this coupon can be used', 'cmb2' ),
        'id'   => $prefix . 'coupon_quantity',
        'type' => 'text',
    ) );
    
}

function set_custom_coupon_columns($columns) {
    unset($columns['author']);
    unset($columns['date']);
    return $columns 
         + array('value' => __('Value'), 
                 'expiration' => __('Expiration'),
                 'quantity' => __('Quantity'));
}

function custom_coupon_column( $column, $post_id ) {
    switch ( $column ) {
        case 'value':
            echo get_post_meta( $post_id , '_wsdev_coupon_value' , true ); 
            break;

        case 'expiration':
            if (get_post_meta( get_the_ID(), '_wsdev_coupon_end_date', true) != '') {
                echo gmdate("m/d/Y", get_post_meta( get_the_ID(), '_wsdev_coupon_end_date', true));
            }
            break;

        case 'quantity':
            echo get_post_meta( $post_id, '_wsdev_coupon_quantity', true);
            break;
    }
}

