<?php
/**
 *
 * WP FrontEnd User Pro
 *
 *
 *
 * Update the date to a timestamp when the form submits
 *
 * @param type $post_id
 */
function convert_date_to_timestamp( $post_id ) {
    
    if ( isset( $_POST['_wsdev_job_open_date'] ) ) {
    	$converted_open_date = strtotime($_POST['_wsdev_job_open_date']);
        update_post_meta( $post_id, '_wsdev_job_open_date', $converted_open_date );
    }

    if ( isset( $_POST['_wsdev_job_close_date'] ) ) {
    	$converted_close_date = strtotime($_POST['_wsdev_job_close_date']);
        update_post_meta( $post_id, '_wsdev_job_close_date', $converted_close_date );
    }
}
 


?>