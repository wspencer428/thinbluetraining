<?php
/**
 * Zip code radius API call
 */

	function zipRadius($zip, $radius) {

		$connect = curl_init('https://www.zipcodeapi.com/rest/1wnOjJvpE6kmFylXJG7VBCEqZ2ewy8HAE5oYOTos0IPFhaD1RLWsnBTnD8TcgBMV/radius.json/'.$zip.'/'.$radius.'/mile');
	    curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($connect, CURLOPT_FRESH_CONNECT, true);
	    curl_setopt($connect, CURLOPT_TIMEOUT, 180);
	    $response_json = '';
	    if ( ($response_json = curl_exec($connect) ) === false) {

	        echo 'Curl error: ' . curl_error($connect);
	        
	    }

	    else {
	        curl_close($connect);
	        return json_decode($response_json);
	    }
	}
?>