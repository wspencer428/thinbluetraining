<?php


function sendSMS($members, $event_id) {

	if (!$members || !$event_id) {
		echo "There's been an error";
		return;
	}

	$link = get_permalink($event_id);
	
    require "Twilio.php";
 
    $AccountSid = "AC5a00b1879d05aed1960f5d2e67677667";
	$AuthToken = "9bdb79ce8beb4f71429a4465bdda0c0f";
 
    $client = new Services_Twilio($AccountSid, $AuthToken);
 
    foreach ($members as $number => $name) {
 
        $sms = $client->account->messages->sendMessage(
 			
 			//from
            "+12252248351", 
 
            // the number we are sending to - Any phone number
            $number,
 
            // the sms body
            //"Hey $name, a new training event has been posted close to you. Click the following link for details! ".$link
            "Hey ".$name.", a new law enforcement training class has been listed in an area near you. "."\r\n".$link."\r\n".'You can change your account preferences at any time by going to http://www.thinbluetraining.com and clicking the user icon in the upper right hand corner to login.'
        );
 
        // Display a confirmation message on the screen
        //echo "Sent message to $name";
    }

}

function wsdev_send_sms_for_event( $post_id ) {

   
    // If this isn't an 'event' post, don't bother working through the rest of this function
    // if ( $post->post_type  != 'event') {
    //     return;
    // }
    // if ($update === true) {
    // 	return;
    // }

    // get the zip code of the event
    $zip = get_post_meta($post_id, '_wsdev_event_zip', true);
    $event_id = $post_id;
    $link = get_permalink($post_id);

    if ($zip == '' || !$zip) {
    	return;
    }


    // get zip codes within 25, 50 and 100 mile radius 

    $zips_25 = [];
    $zips_50 = [];
    $zips_100 = [];
    $zips_250 = [];
    $zips_500 = [];

    $zips_25_json = zipRadius($zip, 25);
    foreach($zips_25_json->zip_codes as $zipcode) {
        array_push($zips_25, $zipcode->zip_code);
    }

    $zips_50_json = zipRadius($zip, 50);
    foreach($zips_50_json->zip_codes as $zipcode) {
        array_push($zips_50, $zipcode->zip_code);
    }

    $zips_100_json = zipRadius($zip, 100);
    foreach($zips_100_json->zip_codes as $zipcode) {
        array_push($zips_100, $zipcode->zip_code);
    }

    $zips_250_json = zipRadius($zip, 250);
    foreach($zips_250_json->zip_codes as $zipcode) {
        array_push($zips_250, $zipcode->zip_code);
    }

    $zips_500_json = zipRadius($zip, 500);
    foreach($zips_500_json->zip_codes as $zipcode) {
        array_push($zips_500, $zipcode->zip_code);
    }

    // get the users by radius preference
    $users_25 = [];
    $users_50 = [];
    $users_100 = [];
    $users_250 = [];
    $users_500 = [];

    $args25 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 25,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_25_query = new WP_User_Query($args25);
    $users_25 = $users_25_query->get_results();

    $args50 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 50,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_50_query = new WP_User_Query($args50);
    $users_50 = $users_50_query->get_results();

    $args100 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 100,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_100_query = new WP_User_Query($args100);
    $users_100 = $users_100_query->get_results();

    $args250 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 250,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_250_query = new WP_User_Query($args250);
    $users_250 = $users_250_query->get_results();

    $args500 = array(
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sms_alert',
				'value'   => 'on',
				'compare' => '='
			),
			array(
				'key'     => 'alert_radius',
				'value'   => 500,
				'type'    => 'numeric',
				'compare' => '='
			)
		),
		'fields' => 'ID'
	 );
    $users_500_query = new WP_User_Query($args500);
    $users_500 = $users_500_query->get_results();

    $targeted_members = [];

    foreach ($users_25 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_25 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_50 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_50 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_100 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_100 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_250 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_250 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    foreach ($users_500 as $user) {
    	if ( in_array( get_the_author_meta( 'user_zip', $user ), $zips_500 ) ) {
    		array_push( $targeted_members, $user );
    	}
    }

    // loop through the targeted members by ID to get their phone numbers and names
    $members = [];
    $targeted_members = array_unique($targeted_members);
    foreach($targeted_members as $member) {
    	if ( get_the_author_meta( 'user_phone', $member ) == '' || !get_the_author_meta( 'user_phone', $member ) ) {
    		continue;
    	}
    	$member_phone = get_the_author_meta( 'user_phone', $member );
    	if ( !get_the_author_meta( 'first_name', $member ) || get_the_author_meta( 'first_name', $member ) == '' ) {
    		$member_name = get_the_author_meta( 'display_name', $member );
    	}
    	else {
    		$member_name = get_the_author_meta( 'first_name', $member );
    	}
    	$members[$member_phone] = $member_name;
    }


    // send the email notifications
    if (!empty($targed_members)) {
        emailAlert($targeted_members, $event_id);
    }

    // send the text messages
    if (!empty($members)) {
        sendSMS($members, $event_id);
    }

    

}

