<?

/**
 * Email notifications
 */

function emailAlert($targeted_members, $event_id) {
	
	if (!$targeted_members) {
		return;
	}

	// $myPost = get_post($event_id);
	// if ($myPost->post_date_gmt != $myPost->post_modified_gmt) {
	// 	return;
	// }

	$emails = [];
	foreach ($targeted_members as $user) {
    	if ( get_the_author_meta( 'user_email', $user ) ) {
    		$address = get_the_author_meta( 'user_email', $user );
	    	array_push( $emails, $address );
    	}
    }
    $names = [];
    foreach ($targeted_members as $user) {
    	if ( get_the_author_meta( 'first_name', $user ) ) {
    		$name = get_the_author_meta( 'first_name', $user );
	    	array_push( $names, $name );
    	}
    	else {
    		$name = get_the_author_meta( 'display_name', $user );
	    	array_push( $names, $name );
    	}
    }

    $link = get_permalink($event_id);
    $title = get_the_title($event_id);
    $date = gmdate("m/d/Y", get_post_meta($event_id, '_wsdev_event_date', true));
	$address = get_post_meta( $event_id, '_wsdev_event_address', true );
	$city = get_post_meta( $event_id, '_wsdev_event_city', true );
	$state = get_post_meta( $event_id, '_wsdev_event_state', true );
	$zip = get_post_meta( $event_id, '_wsdev_event_zip', true );
	$cost = get_post_meta( $event_id, '_wsdev_event_cost', true );
	$description = get_the_content($event_id);

	$subject = 'Training Alert - Thin Blue Training';
	//$body = 'A new training event has been posted close to you. Click the following link for details! '.$link;
	$body = $name. ', great news! A new law enforcement training class has been listed in an area near you. Per your request, here are the details:'."\r\n".

		$title."\r\n".
		$date."\r\n".
		$address.' '.$city.', '.$state.' '.$zip."\r\n".
		'$'.$cost."\r\n".
		$description."\r\n".
		$link."\r\n"."\r\n".

		'For more information, head on over to http://thinbluetraining.com where you can see our full list of courses. Stay safe!'."\r\n"."\r\n"."\r\n".
		'Martin Holloway'."\r\n".
		'thinbluetraining.com'."\r\n"."\r\n".

		'You can change your account preferences at any time by going to http://www.thinbluetraining.com and clicking the user icon in the upper right hand corner to login.';

	// $headers[] = array('Content-Type: text/html; charset=UTF-8');
	//$headers[] = array('Content-Type: text/html; charset=UTF-8');
	$headers[] = 'From: Thin Blue Training <contact@thinbluetraining.com>' . "\r\n";

	wp_mail( $emails, $subject, $body, $headers );

}

function emailJob($job_id) {
	$link = get_permalink($job_id);
    $title = get_the_title($job_id);
    
	$subject = 'New Job Posting - Thin Blue Training';
	//$body = 'A new training event has been posted close to you. Click the following link for details! '.$link;
	$body = 'A new job has been posted on Thin Blue Training.'."\r\n".

		$title."\r\n".
		$link."\r\n"."\r\n".

		'To approve this listing, go to http://www.thinbluetraining.com/wp-admin to get to the Dashboard. Then click Jobs -> All Jobs. This job ('.$title.') will be listed as "Pending". To approve the post, just click "Publish".'."\r\n"."\r\n"."\r\n".

	// $headers[] = array('Content-Type: text/html; charset=UTF-8');
	//$headers[] = array('Content-Type: text/html; charset=UTF-8');
	$headers[] = 'From: Thin Blue Training <contact@thinbluetraining.com>' . "\r\n";

	wp_mail( 'wspencer428@hotmail.com', $subject, $body, $headers );
}