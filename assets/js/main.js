(function($) {
  $('.navbar-nav a').hover(function() {
    $(this).find('span').toggle();
  });
})( window.jQuery || window.Zepto );

(function( $ ){

  $('#month_select').change(function() {
		$('#month_form').submit();
	});

  $('#topic_select').change(function() {
		$('#topic_form').submit();
	});

  $('#state_select').change(function() {
		// $('#state_form_calendar').submit();
    $(this).closest('form').submit();
	});

  $('#searchButton').click(function(event) {
    event.preventDefault();
    $('#zip_form').submit();
  });


})( window.jQuery || window.Zepto );


(function($) {
	$('#filter').keyup(function () {

        var rex = new RegExp($(this).val(), 'i');
        $('.searchable tr').not('.table-month-divider').hide();
        $('.searchable tr').not('.table-month-divider').filter(function () {
            return rex.test($(this).text());
        }).show();

    });

    

})(window.jQuery || window.Zepto);

(function($) {
  $('#account-tab').click(function() {
    if ($('#account-tab-list').is(":hidden")) {
      $('#account-tab-list').slideDown();
      $('#account-tab .fa-angle-up').toggle();
      $('#account-tab .fa-angle-down').toggle();
    }
    else {
      $('#account-tab-list').slideUp();
      $('#account-tab .fa-angle-up').toggle();
      $('#account-tab .fa-angle-down').toggle();
    }
    
  });

  $('#account-tab-list').mouseout(function() {
    if($('#account-tab-list').children().is(':hover') || $('#account-tab-list').is(':hover')) {
      return;
    }
    else {
      $('#account-tab-list').slideUp();
      $('#account-tab .fa-angle-up').toggle();
      $('#account-tab .fa-angle-down').toggle();
    }
  });
})(window.jQuery || window.Zepto);

(function($) {
  $(document).ready(function() {
    $('#_wsdev_job_close_date, #_wsdev_job_open_date').datepicker({
      format: 'mm/dd/yyyy',
      startDate: '-3d'
    });
  });
})(window.jQuery || window.Zepto);

(function($) {
  $(document).ready(function() {
    $('#_wsdev_event_date, #_wsdev_event_date_end').datepicker({
      format: 'mm/dd/yyyy',
      startDate: '-3d'
    });
  });
})(window.jQuery || window.Zepto);

(function($) {
  $('.panel-heading a').click(function() {
    if ($(this).find('.fa-minus').is(':visible')) {
      $(this).find('.fa-minus').hide();
        $(this).find('.fa-plus').show();             
        
    }
    else {
      $(this).find('.fa-minus').show();
        $(this).find('.fa-plus').hide();

    }

    $('.panel-heading a').not(this).find('.fa-minus').hide();
      $('.panel-heading a').not(this).find('.fa-plus').show();
    
  });
})(window.jQuery || window.Zepto);

(function($) {
  $(document).ready(function() {
    if (window.location.pathname.indexOf('recruiter') > -1) {
      $('input#user_type').val('recruiter');
    }
    else {
      $('input#user_type').val('trainer');
    }
  });
})(window.jQuery || window.Zepto);