<?php
/**
 * bootstrapped functions and definitions
 *
 * @package bootstrapped
 */

/****************************************
Theme Setup
*****************************************/

/**
 * Theme initialization - functions are defined in /inc/theme-functions.php
 */
require get_template_directory() . '/inc/init.php';

/**
 * Custom theme functions initialized with actions/filters in /inc/init.php
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Member functions
 */
require get_template_directory() . '/inc/members.php';

/**
 * Trainer functions
 */
require get_template_directory() . '/inc/trainers.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/cpt/event.php';
require get_template_directory() . '/inc/cpt/job.php';
require get_template_directory() . '/inc/cpt/coupon.php';

/**
 * Plugin Customizations
 */
require get_template_directory() . '/inc/plugins/wp-frontend-pro.php';

/**
 * APIs
 */
require get_template_directory() . '/inc/api/email-alerts.php';
require get_template_directory() . '/inc/api/zip-radius.php';
require get_template_directory() . '/inc/api/twilio-send.php';
require get_template_directory() . '/inc/api/aweber.php';