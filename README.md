# Bootstrapped

Version: 2.2.0

## Author:

Will Spencer ( [@BetterOffDev](http://twitter.com/BetterOffDev) )

## Summary

This will be the repository for all code related to ThinBlueTraining.com. This is a private repository, meaning it cannot be searched for. I've provided you the direct link so you can track code changes if you'd like. 

You don't have to understand the code to follow along. If you check the "Commits" link, you'll see every time I've pushed new code to the repository. I try to accompany these code updates with notes that detail the changes made. 

This repository also acts as an additional backup point for the website code. As content starts being added to the site, I plan on backing up the database to a DropBox account. We can additionally place database backups in other locations, too.

#### Releases
* 9/27/15 - Release 2.2.0
   Removed pay-per-post functionality
   Gave trainers the ability to edit/delete previous class listings
* 9/22/15 - Release 2.1.1
   Updated mobile styling
* 9/21/15 - Release 2.1.0
   Finalized coupon code functionality
* 9/18/15 - Release 2.0.0
   All code from this point on is officially in production
* 7/15/15 - Release 0.0.1
   Initial commit of WP theme template