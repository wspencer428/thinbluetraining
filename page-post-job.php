<?php
/**
 * page-post-job.php
 *
 * @package bootstrapped
 */

date_default_timezone_set('America/New_York');

if ( !isset($_GET['post']) && isset($_POST['submit']) ) {
   
   // we're creating a new post
   if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "job_posted") {

      if (wp_verify_nonce($_POST['job_posted'], 'jobPost')) {

         // Do some minor form validation to make sure there is content
         if (isset($_POST['submit'])) {

            $current_user = wp_get_current_user();  
            
            //Check to make sure that the title field is not empty
            if ($_POST['job_title'] === '') {
               $titleError = 'You forgot to enter a title.';
               $hasError = true;    
            }

            else {
               $job_title = $_POST['job_title'];
            }

            //Check to make sure content was entered
            if ($_POST['job_description'] === '') {
               $descriptionError = 'You forgot to enter your job description.';
               $hasError = true;
            }

            else {
               $job_description = $_POST['job_description'];
            }

            //Check to make sure an agency was entered
            if ($_POST['_wsdev_job_agency'] === '') {
               $agencyError = 'You forgot to enter hiring agency.';
               $hasError = true;
            }

            else {
               $job_agency = $_POST['_wsdev_job_agency'];
            }

            //Check to make sure a city was entered
            if ($_POST['_wsdev_job_city'] === '') {
               $cityError = 'You forgot to enter a city.';
               $hasError = true;
            }

            else {
               $job_city = $_POST['_wsdev_job_city'];
            }

            //Check to make sure a state was selected
            if ($_POST['_wsdev_job_state'] === '') {
               $stateError = 'You forgot to select a state.';
               $hasError = true;
            }

            else {
               $job_state = $_POST['_wsdev_job_state'];
            }

            //Check to make sure a zipcode was entered
            if ($_POST['_wsdev_job_zip'] === '') {
               $zipError = 'You forgot to select a state.';
               $hasError = true;
            }

            else {
               $job_zip = $_POST['_wsdev_job_zip'];
            }

            //Check to make sure a website was entered
            if ($_POST['_wsdev_job_website'] === '') {
               $websiteError = 'You forgot to enter a website.';
               $hasError = true;
            }

            else {
               $job_website = $_POST['_wsdev_job_website'];
            }

            //Check to make sure a job open date was entered
            if ($_POST['_wsdev_job_open_date'] === '') {
               $openDateError = 'You forgot to job opening date.';
               $hasError = true;
            }

            else {
               $job_open_date = $_POST['_wsdev_job_open_date'];
            }

            //Check to make sure a job close date was entered
            if ($_POST['_wsdev_job_close_date'] === '') {
               $closeDateError = 'You forgot to enter a job closing date.';
               $hasError = true;
            }

            else {
               $job_close_date = $_POST['_wsdev_job_close_date'];
            }

            if ($_POST['_wsdev_job_benefits'] === '') {
               $job_benefits = '';
            }

            else {
               $job_benefits = $_POST['_wsdev_job_benefits'];
            }

            
            if ($_POST['_wsdev_job_contact_person'] === '') {
               $job_contact_person = '';
            }

            else {
               $job_contact_person = $_POST['_wsdev_job_contact_person'];
            }

            
            if ($_POST['_wsdev_job_contact_email'] === '') {
               $job_contact_email = '';
            }

            else {
               $job_contact_email = $_POST['_wsdev_job_email'];
            }

            if ($_POST['_wsdev_job_contact_phone'] === '') {
               $job_contact_phone = '';
            }

            else {
               $job_contact_phone = $_POST['_wsdev_job_phone'];
            }

            if (!isset($hasError)) {

               // Add the form input to $updated_post array
               if (empty($error)) {

                  $new_post = array(
                     'post_title'  =>  wp_strip_all_tags( $_POST['job_title'] ),
                     'post_content'    =>  $_POST[ 'job_description' ],
                     'post_author' => $current_user->ID,
                     'post_name' => sanitize_title( $_POST['job_title'] ),
                     'post_date' => date('Y-m-d H:i:s'),
                     'post_date_gmt' => date('Y-m-d H:i:s', strtotime('+4 hours')),
                     'post_status' =>  'pending',  // Choose: publish, preview, future, draft, etc.
                     'post_type'      =>   'job'  //'post','page' or CPT
                  );

               }
            }
         }
      }

      else {
         $nonceError = 'Invalid nonce.';
         $hasError = true;
      }

      $post_id = wp_insert_post($new_post);


      if (isset($_POST['_wsdev_job_agency'])) {

      	$job_agency = $_POST['_wsdev_job_agency'];
      	if (get_post_meta($post_id, '_wsdev_job_agency', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_agency', $job_agency);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_agency', $job_agency);
        }

      }

      if (isset($_POST['_wsdev_job_city'])) {

      	$job_city = $_POST['_wsdev_job_city'];
      	if (get_post_meta($post_id, '_wsdev_job_city', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_city', $job_city);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_city', $job_city);
        }

      }

      if (isset($_POST['_wsdev_job_state'])) {

      	$job_state = $_POST['_wsdev_job_state'];
      	if (get_post_meta($post_id, '_wsdev_job_state', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_state', $job_state);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_state', $job_state);
        }

      }

      if (isset($_POST['_wsdev_job_zip'])) {

      	$job_zip = $_POST['_wsdev_job_zip'];
      	$zipNumeric = preg_replace('/\D/', '', $job_zip);
      	if (get_post_meta($post_id, '_wsdev_job_zip', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_zip', $zipNumeric);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_zip', $zipNumeric);
        }

      }

      if (isset($_POST['_wsdev_job_website'])) {

      	$job_website = $_POST['_wsdev_job_website'];
      	if (get_post_meta($post_id, '_wsdev_job_website', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_website', $job_website);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_website', $job_website);
        }

      }

      if (isset($_POST['_wsdev_job_open_date'])) {

      	$job_open_date = strtotime($_POST['_wsdev_job_open_date']);
      	if (get_post_meta($post_id, '_wsdev_job_open_date', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_open_date', $job_open_date);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_open_date', $job_open_date);
        }

      }

      if (isset($_POST['_wsdev_job_close_date'])) {

      	$job_close_date = strtotime($_POST['_wsdev_job_close_date']);
      	if (get_post_meta($post_id, '_wsdev_job_close_date', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_close_date', $job_close_date);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_close_date', $job_close_date);
        }

      }

      if (isset($_POST['_wsdev_job_benefits'])) {

      	$job_benefits = $_POST['_wsdev_job_benefits'];
      	if (get_post_meta($post_id, '_wsdev_job_benefits', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_benefits', $job_benefits);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_benefits', $job_benefits);
        }

      }

      if (isset($_POST['_wsdev_job_contact_person'])) {

      	$job_contact = $_POST['_wsdev_job_contact_person'];
      	if (get_post_meta($post_id, '_wsdev_job_contact_person', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_contact_person', $job_contact);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_contact_person', $job_contact);
        }

      }

      if (isset($_POST['_wsdev_job_contact_email'])) {

      	$job_contact_email = $_POST['_wsdev_job_contact_email'];
      	if (get_post_meta($post_id, '_wsdev_job_contact_email', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_contact_email', $job_contact_email);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_contact_email', $job_contact_email);
        }

      }

      if (isset($_POST['_wsdev_job_contact_phone'])) {

      	$job_contact_phone = $_POST['_wsdev_job_contact_phone'];
      	$phoneNumeric = preg_replace('/\D/', '', $job_contact_phone);
      	if (get_post_meta($post_id, '_wsdev_job_contact_phone', FALSE)) { 
            update_post_meta($post_id, '_wsdev_job_contact_phone', $phoneNumeric);
        } 
        else { 
           add_post_meta($post_id, '_wsdev_job_contact_phone', $phoneNumeric);
        }

      }

      if($post_id) {
        $author = get_post_field( 'post_author', $post_id );
        $author_name = get_the_author_meta('user_login', $author);
        emailJob($post_id);
        wp_redirect( get_permalink(110) );
      }

   }
}

if ( !is_user_logged_in() ) {
    header('Location: ' . get_permalink(225));
} 

get_header(); 

 ?>
	<div class="row">
		<div class="col-md-7">
            <h1 class="page-title">Post a new job</h1> 
            
            <form action="" method="post">
            	<div class="form-group">
            		<?php if($titleError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $titleError;?></div>
                  	<?php } ?>
            		<label for="job_title">Job Title <span class="required">*</span></label>
            		<input class="form-control" id="job_title" type="text" name="job_title" placeholder="" value="<?php echo $job_title; ?>" size="65">
        			<span class="help-block">Give your job a descriptive title to help it stand out</span>
        		</div>

        		<div class="form-group">
        			<?php if($descriptionError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $descriptionError;?></div>
                  	<?php } ?>
        			<label for="job_description">Job Description <span class="required">*</span></label>
        			<textarea class="form-control" id="job_description" name="job_description" placeholder="" rows="5" cols="25"><?php echo $job_description; ?></textarea>
                    <span class="help-block">A brief description of the job opportunity</span>
                </div>

                <div class="form-group">
        			<label for="_wsdev_job_benefits">Salary/Benefits</label>
        			<textarea class="form-control" id="_wsdev_job_benefits" name="_wsdev_job_benefits" placeholder="" rows="5" cols="25"><?php echo $job_benefits; ?></textarea>
                    <span class="help-block"></span>
                </div>

                <div class="form-group">
                	<?php if($agencyError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $agencyError;?></div>
                  	<?php } ?>
                	<label for="_wsdev_job_agency">Hiring Agency <span class="required">*</span></label>
                	<input class="form-control" id="_wsdev_job_agency" type="text" name="_wsdev_job_agency" placeholder="" value="<?php echo $job_agency; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($cityError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $cityError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_city">City <span class="required">*</span></label>
        			<input class="form-control" id="_wsdev_job_city" type="text" name="_wsdev_job_city" placeholder="" value="<?php echo $job_city; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($stateError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $stateError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_state">State <span class="required">*</span></label>
        			<select class="form-control" name="_wsdev_job_state">

                        <option value=""> - select state -</option>
                        <option value="AL">AL</option>
                        <option value="AK">AK</option>
                        <option value="AR">AR</option>
                        <option value="AZ">AZ</option>
                        <option value="CA">CA</option>
                        <option value="CO">CO</option>
                        <option value="CT">CT</option>
                        <option value="DC">DC</option>
                        <option value="DE">DE</option>
                        <option value="FL">FL</option>
                        <option value="GA">GA</option>
                        <option value="HI">HI</option>
                        <option value="IA">IA</option>
                        <option value="ID">ID</option>
                        <option value="IL">IL</option>
                        <option value="IN">IN</option>
                        <option value="KS">KS</option>
                        <option value="KY">KY</option>
                        <option value="LA">LA</option>
                        <option value="MA">MA</option>
                        <option value="MD">MD</option>
                        <option value="ME">ME</option>
                        <option value="MI">MI</option>
                        <option value="MN">MN</option>
                        <option value="MO">MO</option>
                        <option value="MS">MS</option>
                        <option value="MT">MT</option>
                        <option value="NC">NC</option>
                        <option value="ND">ND</option>
                        <option value="NE">NE</option>
                        <option value="NH">NH</option>
                        <option value="NJ">NJ</option>
                        <option value="NM">NM</option>
                        <option value="NV">NV</option>
                        <option value="NY">NY</option>
                        <option value="OH">OH</option>
                        <option value="OK">OK</option>
                        <option value="OR">OR</option>
                        <option value="PA">PA</option>
                        <option value="RI">RI</option>
                        <option value="SC">SC</option>
                        <option value="SD">SD</option>
                        <option value="TN">TN</option>
                        <option value="TX">TX</option>
                        <option value="UT">UT</option>
                        <option value="VA">VA</option>
                        <option value="VT">VT</option>
                        <option value="WA">WA</option>
                        <option value="WI">WI</option>
                        <option value="WV">WV</option>
                        <option value="WY">WY</option>
                    </select>
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($zipError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $zipError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_zip">Zip Code <span class="required">*</span></label>
        			<input class="form-control" id="_wsdev_job_zip" type="text" name="_wsdev_job_zip" placeholder="" value="<?php echo $job_zip; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($websiteError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $websiteError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_website">Website <span class="required">*</span></label>
        			<input id="wpuf-_wsdev_job_website" type="url" class="form-control" name="_wsdev_job_website" placeholder="" value="<?php echo $job_website; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($openDateError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $openDateError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_open_date">Job Open Date <span class="required">*</span></label>
        			<input id="_wsdev_job_open_date" type="text" class="form-control" name="_wsdev_job_open_date" value="<?php echo $job_open_date; ?>" size="30">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<?php if($closeDateError != '') { ?>
                     	<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $closeDateError;?></div>
                  	<?php } ?>
        			<label for="_wsdev_job_close_date">Job Closing Date <span class="required">*</span></label>
        			<input id="_wsdev_job_close_date" type="text" class="form-control" name="_wsdev_job_close_date" value="<?php echo $job_close_date; ?>" size="30">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<label for="_wsdev_job_contact">Contact Person</label>
        			<input class="form-control" id="_wsdev_job_contact" type="text" name="_wsdev_job_contact_person" placeholder="" value="<?php echo $job_contact_person; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<label for="_wsdev_job_contact_email">Contact E-mail</label>
        			<input id="_wsdev_job_contact_email" type="email" class="form-control" name="_wsdev_job_contact_email" placeholder="" value="<?php echo $job_contact_email; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<div class="form-group">
        			<label for="_wsdev_job_contact_phone">Contact Phone</label>
        			<input class="form-control" id="_wsdev_job_contact_phone" type="text" name="_wsdev_job_contact_phone" placeholder="" value="<?php echo $job_contact_phone; ?>" size="40">
        			<span class="help-block"></span>
        		</div>

        		<input type="hidden" name="action" value="job_posted" />
                <?php wp_nonce_field( 'jobPost', 'job_posted' ); ?>


        		<div class="form-group">
        			<input type="submit" name="submit" class="btn btn-primary btn-lg" value="Submit Job">
        		</div>

        		

        	</form>

        	
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>