<?php
/**
 * page-registration.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            <?php the_content(); ?>
	            <!-- <h4 class="p1">Thin Blue Training is here to serve officers, trainers and recruiters. Signing up is simple and you’ll be able to take advantage of all of our great features from <span style="text-decoration: underline;">one single account.</span></h4>
				<h2 class="p1"><span class="s1"><b><span style="text-decoration: underline;">Officers</span></b></span></h2> -->
				<h3 class="p1"><span class="s1"><b>ALLOW US TO DO THE </b></span><span class="s1"><b>WORK FOR YOU!</b></span></h3>
				<p class="p3"><span class="s1"><b>3 Reasons to Subscribe to Thin Blue Training Alerts:</b></span></p>
				<p class="p6"><span class="s1"><b>Instant Alerts! </b>Get instant alerts to training as soon as it’s listed.</span></p>
				<p class="p6"><span class="s1"><b>Tailored to Fit.</b> Choose the area you want to be alerted about when a new class is listed. Want to hear about training within hundreds of miles? Maybe only as close as the next town over? We’ve got you covered.</span></p>
				<p class="p6"><span class="s1"><b>You’re the Boss.</b> Want to be notified by Text? Email? Both? You get to select how you’re alerted.</span></p>
				<p style="text-align: center;"><a href="<?php bloginfo('url'); ?>/member-registration" class="btn btn-primary btn-lg">Register for Alerts</a></p>
				<!-- <h2 class="p6"><span style="text-decoration: underline;"><strong>Trainers</strong></span></h2>
				<h3 class="p6"><strong>ARE YOU A TRAINER LOOKING TO LIST CLASSES?</strong></h3>
				<p class="p1"><span class="s1">List your class with us and it stays on our calendar <b>until your class is concluded</b>. It’s a low, one-time fee for each class.</span></p>
				<p>Increase your exposure and you’ll increase your attendance. Want to list multiple classes? You get a discount. The breakdown is as follows:</p>
				<p class="p1"><span class="s1">1-5 classes $35 per class</span></p>
				<p class="p1"><span class="s1">6-20 classes $30 per class</span></p>
				<p class="p1"><span class="s1">21 + classes $20 per class</span></p>
				<p class="p1"><span class="s1">In order to list your class, you only have to register. Click the button below to get started.</span></p>
				<p style="text-align: center;"><a href="<?php bloginfo('url'); ?>/trainer-registration" class="btn btn-primary btn-lg">Register as a Trainer</a></p>
				<h2 class="p1"><strong><span style="text-decoration: underline;">Recruiters</span></strong></h2>
				<h3 class="p1"><strong><span class="s1">WANT TO INCREASE YOUR NUMBER OF APPLICANTS? LIST YOUR CLASS HERE!</span></strong></h3>
				<p class="p1"><span class="s1">It’s completely free, now and always. Our goal is simple: to give officers opportunities.</span></p>
				<p class="p1"><span class="s1">In order to list your class, simply register by clicking the button below. We won’t spam you; it’s just how we avoid receiving spam listings here.</span></p>
				<p style="text-align: center;"><a href="<?php bloginfo('url'); ?>/recruiter-registration" class="btn btn-primary btn-lg">Register as a Recruiter</a></p> -->
				
        	        
        	<?php endwhile; ?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>