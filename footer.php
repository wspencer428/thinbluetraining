<?php
/**
 * footer.php
 *
 * Includes closing of main content, footer content and closing of <body> & <html>
 *
 * @package bootstrapped
 */
?>

		</div><!-- #main_content -->
	</div><!--#wrap-->
	<footer>
		<div class="container">
			<div class="site-info row">
				<div class="col-sm-4">
					<p class="copyright"><img src="<?php bloginfo('stylesheet_directory'); ?>/dist/img/logo-icon.png" />&nbsp;&copy; <?php echo date( "Y" ); echo " "; bloginfo( 'name' ); ?></p>
				</div>
				<div class="col-sm-8">
					<ul class="disclaimers" style="list-style-type: none;">
						<li style="display: inline;"><a href="<?php bloginfo('url'); ?>/affiliate-disclaimer">Affiliate Disclaimer</a></li>
						<li style="display: inline;"><a href="<?php bloginfo('url'); ?>/dmca-policy">DMCA Policy</li>
						<li style="display: inline;"><a href="<?php bloginfo('url'); ?>/privacy-policy">Privacy Policy</li>
						<li style="display: inline;"><a href="<?php bloginfo('url'); ?>/terms-of-use">Terms of Use</li>
						<li style="display: inline;"><a href="<?php bloginfo('url'); ?>/advertise-with-us">Advertise With Us</li>
					</ul>
				</div>
			</div><!-- .site-info -->
		</div>
	</footer>
<?php wp_footer(); ?>
<!-- All development, design and functionality built by Will Spencer -->
</body>
</html>
