<?php
/**
 * single-job.php
 *
 * @package bootstrapped
 */

get_header(); ?>
	<div class="row">
		<div class="col-md-7">
          	<?php while (have_posts()) : the_post(); ?>
	            <h1 class="page-title"><?php the_title(); ?></h1> 
	            <strong>Description: </strong><?php the_content(); ?>
	            <?php
	            	$benefits = get_post_meta( get_the_ID(), '_wsdev_job_benefits', true );
	            	$open_date = gmdate("m/d/Y", get_post_meta( get_the_ID(), '_wsdev_job_open_date', true));
	            	$close_date = gmdate("m/d/Y", get_post_meta( get_the_ID(), '_wsdev_job_close_date', true));
	            	$agency = get_post_meta( get_the_ID(), '_wsdev_job_agency', true );
	            	$city = get_post_meta( get_the_ID(), '_wsdev_job_city', true );
	            	$state = get_post_meta( get_the_ID(), '_wsdev_job_state', true );
	            	$zip = get_post_meta( get_the_ID(), '_wsdev_job_zip', true );
	            	$website = get_post_meta( get_the_ID(), '_wsdev_job_website', true );
	            	$contact_person = get_post_meta( get_the_ID(), '_wsdev_job_contact_person', true );
	            	$contact_email = get_post_meta( get_the_ID(), '_wsdev_job_contact_email', true );
	            	$contact_phone = get_post_meta( get_the_ID(), '_wsdev_job_contact_phone', true );
	            ?>
	            <strong>Salary/Benefits: </strong><?php echo $benefits; ?><br />
	            <ul class="details-list">
	            	<li><strong>Agency: </strong><?php echo $agency; ?></li>
	            	<li><strong>Location: </strong><?php echo $city; ?>&nbsp;<?php echo $state; ?>,&nbsp;<?php echo $zip; ?></li>
	            	<li><strong>Open Date: </strong><?php echo $open_date; ?></li>
	            	<li><strong>Close Date: </strong><?php echo $close_date; ?></li>
	            	<li><strong>Website: </strong><a href="<?php echo $website; ?>"><?php echo $website; ?></a></li>
	            	<li><strong>Contact Person: </strong><?php echo $contact_person; ?></li>
	            	<li><strong>Contact E-mail: </strong><?php echo $contact_email; ?></li>
	            	<li><strong>Contact Phone: </strong><?php echo $contact_phone; ?></li>
	            </ul>
        	<?php endwhile; ?>
        </div>

        <?php get_sidebar(); ?>
	</div>


<?php get_footer(); ?>